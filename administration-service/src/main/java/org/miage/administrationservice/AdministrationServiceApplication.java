package org.miage.administrationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class AdministrationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdministrationServiceApplication.class, args);
	}
	
	@Bean
	// execute et enregistre la valeur de retour en tant que bean
	//Ici on retourne une doc générer avec la méthode OpenApi
	//http://localhost:8084/v3/api-docs/
	public OpenAPI customOpenApi() {
		
		return new OpenAPI().info(new Info().title("Administration API").version("1.0")
				.description("Documentation Administration API MIAGE 2 - SID"));
	}

}
