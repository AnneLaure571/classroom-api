package org.miage.administrationservice.boundary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Erreur : il existe déjà un nom existant avec cet épisode")
public class EpisodeExistsException extends RuntimeException {

}

