package org.miage.administrationservice.boundary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Vous ne disposez pas des droits nécessaires !")
public class UnauthorizedUserException extends RuntimeException {

}
