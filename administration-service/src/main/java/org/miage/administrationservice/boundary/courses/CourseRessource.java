package org.miage.administrationservice.boundary.courses;

import java.util.List;

import org.miage.administrationservice.entity.courses.*;
import org.miage.administrationservice.entity.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(collectionResourceRel="course")

public interface CourseRessource extends JpaRepository<Course, String>{
	//PUT, GET, POST, DELETE, PATCH sont gérées automatiquement
	
	Iterable<Course> findByNom(@Param("nom") String  nom);
	
	Iterable<Course> findByTheme(@Param("theme") String  theme);

	Iterable<Course> findByNomAndTheme(@Param("nom")String nom, @Param("theme")String theme);

	Iterable<Course> findAllByNomAndThemeAndPrixAndStatutIn(@Param("nom") String nom,@Param("theme") String theme,@Param("prix") Double prix,@Param("statut") List<String> statut);

	Iterable<Course> findAllByNomAndThemeAndStatutIn(@Param("nom") String nom,@Param("theme") String theme,@Param("statut") List<String> statut);

	Iterable<Course> findAllByNomAndStatutIn(String nom, List<String> statut);

	Iterable<Course> findByNomAndThemeAndPrix(String nom, String theme, Double prix);

	Iterable<Course> findAllByThemeAndStatutIn(String theme, List<String> statut);

	Iterable<Course> findAllByStatutIn(List<String> statut);

	Iterable<Course> findByNomOrTheme(String nom, String theme);
	
	/*Iterable<Course> findAllByStatutIn(@Param("statut") List<String>  statut);
	
	Iterable<Course> findAllByNomAndStatutIn(@Param("nom") String  nom, @Param("statut") List<String> statut);*/

}
