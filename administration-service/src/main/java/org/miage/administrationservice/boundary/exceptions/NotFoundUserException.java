package org.miage.administrationservice.boundary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Aucun utilisateur correspondant !")
public class NotFoundUserException {

}
