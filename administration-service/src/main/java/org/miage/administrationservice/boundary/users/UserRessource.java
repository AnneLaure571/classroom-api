package org.miage.administrationservice.boundary.users;

import java.util.List;
import java.util.Optional;

import org.miage.administrationservice.entity.users.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(collectionResourceRel="users")

public interface UserRessource extends JpaRepository<User, String>{
	//PUT, GET, POST, DELETE, PATCH sont gérées automatiquement
	
	Iterable<User> findByStatut(@Param("statut") String  statut);
	
	Iterable<User> findAllByStatutIn(@Param("statut") List<String>  statut);
	
	Iterable<User> findByNom(@Param("nom") String  nom);
	
	Optional<User> findByEmail(@Param("email") String email);
	
	Iterable<User> findByNomOrPrenom(@Param("nom") String  nom, @Param("prenom") String prenom);
	
	Iterable<User> findByNomAndPrenom(@Param("nom") String  nom, @Param("prenom") String prenom);
	
	Iterable<User> findAllByNomAndStatutIn(@Param("nom") String  nom, @Param("statut") List<String> statut);
	
	Iterable<User> findAllByNomAndPrenomAndStatutIn(@Param("nom") String  nom,@Param("prenom") String prenom, @Param("statut") List<String> statut);

	Iterable<User> findAllByPrenomAndStatutIn(@Param("prenom")String prenom, @Param("nom")List<String> statut);
	
	
	//Iterable<User> findAllByNomInAndPrenomIn(@Param("nom") List<String>  nom, @Param("prenom") List<String> prenom);
}
