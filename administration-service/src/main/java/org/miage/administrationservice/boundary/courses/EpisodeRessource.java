package org.miage.administrationservice.boundary.courses;

import java.util.List;
import java.util.Optional;

import org.miage.administrationservice.entity.courses.Course;
import org.miage.administrationservice.entity.episodes.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(collectionResourceRel="course")

public interface EpisodeRessource extends JpaRepository<Episode, String>{

	Iterable<Episode> findByCourseId(String courseId);
	
	Optional<Episode> findByCourseIdAndId(String courseId, String id);
	
	public Course getByCourse(String courseId);
	
	//PUT, GET, POST, DELETE, PATCH sont gérées automatiquement
	
	//Iterable<User> findByStatut(@Param("statut") String  statut);
	
	/*Iterable<Course> findAllByStatutIn(@Param("statut") List<String>  statut);
	
	Iterable<Course> findByNom(@Param("nom") String  nom);
	
	Iterable<Course> findByNomOrPrenom(@Param("nom") String  nom, @Param("prenom") String prenom);
	
	Iterable<Course> findByNomAndPrenom(@Param("nom") String  nom, @Param("prenom") String prenom);
	
	Iterable<Course> findAllByNomAndStatutIn(@Param("nom") String  nom, @Param("statut") List<String> statut);*/

}