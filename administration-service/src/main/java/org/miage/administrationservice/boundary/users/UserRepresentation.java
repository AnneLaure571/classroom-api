package org.miage.administrationservice.boundary.users;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.miage.administrationservice.boundary.courses.CourseRepresentation;
import org.miage.administrationservice.boundary.exceptions.BadRequestException;
import org.miage.administrationservice.entity.users.*;
import org.miage.administrationservice.entity.users.User;
import org.miage.administrationservice.boundary.users.UserRepresentation;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ReflectionUtils;

@RestController
// not need to define @RequestBody, restcontroller specific version of controller
// RequestMapping, allow to define URI (value), and the result produces
@RequestMapping(value="/utilisateurs", produces= MediaType.APPLICATION_JSON_VALUE)
// HATEOAS create links based on model user
@ExposesResourceFor(User.class)
public class UserRepresentation {
	
	private final UserRessource ur;
	// add validator
	private final UserValidator uv;
	
	public UserRepresentation(UserRessource ur, UserValidator uv) {
        this.ur = ur;
        this.uv = uv;
    }
	
	// GET by filters 
	@GetMapping(value="/login")
	public ResponseEntity<?> login(@RequestParam(value="email") String email){
		
		Optional<User> existingEmail = ur.findByEmail(email);
		//check if email already exists 
		if (!existingEmail.isPresent()) {
			return ResponseEntity.status(404).header("header", "invalid-user").body("Aucun utilisateur correspondant !");
		}
		User user = existingEmail.get();
		if(!user.getRole().equals("ADMIN"))
			return ResponseEntity.status(401).header("header", "unauthorized").body("Vous ne disposez pas des accès suffisants !");
			
		return ResponseEntity.ok(userToResource(user));
	}
	
	// GET all 
	@GetMapping
	public ResponseEntity<?> getAllUsers(){
		// like for each, iterate all element of the Iterable
		Iterable<User> all = ur.findAll();
		return ResponseEntity.ok(userToResource(all));
	}

	// GET one 
	@GetMapping(value="{userId}")
	public ResponseEntity<?> getUser(@PathVariable("userId") String id){
		return Optional.ofNullable(ur.findById(id)).filter(Optional::isPresent)
				//.map(i -> ResponseEntity.ok(i))
				.map(i -> ResponseEntity.ok(userToResource(i.get(), true)))
				.orElse(ResponseEntity.notFound().build());
	}
	
	// GET by filters 
	@GetMapping(value="/")
	public ResponseEntity<?> searchUser(@RequestParam(value="statut", required=false) List<String> statut,
			@RequestParam(value="nom", required=false) String nom, 
			@RequestParam(value="prenom", required=false) String prenom){
		
		Iterable<User> all;
		
		if(nom != null && prenom != null && statut != null) {
			all = ur.findAllByNomAndPrenomAndStatutIn(nom, prenom, statut);
		} else if(nom != null && prenom != null) {
			all = ur.findByNomAndPrenom(nom, prenom);
		} else if(statut != null && nom != null) {
			all = ur.findAllByNomAndStatutIn(nom, statut);
		} else if(statut != null && prenom != null) {
			all = ur.findAllByPrenomAndStatutIn(prenom, statut);
		} else if(statut != null) {
			all = ur.findAllByStatutIn(statut);
		} else {
			all = ur.findByNomOrPrenom(nom,prenom);
		}
		
		return ResponseEntity.ok(userToResource(all));
	}
	
	// POST 
	@PostMapping
	@Transactional
	public ResponseEntity<?> registerUser(@RequestBody @Valid UserInput user) throws BadRequestException{
		Optional<User> existingUser = ur.findByEmail(user.getEmail());
		//check if email already exists 
		if (existingUser.isPresent()) {
			 return ResponseEntity.status(409).header("header", "conflict").body("Un utilisateur existe déjà avec cet email !");
		}
		User u = new User(UUID.randomUUID().toString(), user.getNom(), user.getPrenom(), user.getEmail(),
				user.getRole(), user.getStatut(), user.getPays());
		User saved = ur.save(u);
		// slash allow to add sub resource in URI
		URI location = linkTo(UserRepresentation.class).slash(saved.getId()).toUri();
		return ResponseEntity.status(201).header("location", location.toString()).body(userToResource(saved, true));
	}
	
	//PUT
	@PutMapping(value="/{userId}")
	@Transactional
	public ResponseEntity<?> updateUser(@PathVariable("userId") String userId, @RequestBody @Valid User userUp) throws BadRequestException{
		//Check if ID exist
		Optional<User> body = Optional.ofNullable(userUp);
		if (!ur.existsById(userId) || userUp.getStatut().equals("SUPPRIMÉ")) {
			return ResponseEntity
		      .status(404)
		      .header("header", "user-invalid")
		      .body("L'utilisateur est introuvable ou supprimé !");
		}
		if (!body.isPresent()) {
			return ResponseEntity.badRequest().build();
			
		}
		userUp.setId(userId);
		User result = ur.save(userUp);
		return ResponseEntity.ok(userToResource(result, true));
	}

	// PATCH
    @PatchMapping(value = "/{userId}")
    @Transactional
    public ResponseEntity<?> updateUserPartiel(@PathVariable("userId") String userId,
            @RequestBody @Valid Map<Object, Object> fields){
        Optional<User> body = ur.findById(userId);
        if (body.isPresent()) {
            User user = body.get();
            fields.forEach((f, v) -> {
                Field field = ReflectionUtils.findField(User.class, f.toString());
                field.setAccessible(true);
                ReflectionUtils.setField(field, user, v);
            });
            if (!ur.existsById(userId) || user.getStatut().equals("SUPPRIMÉ")) {
            	return ResponseEntity
          		      .status(404)
          		      .header("header", "user-invalid")
          		      .body("L'utilisateur est introuvable ou supprimé !");
    		}
            uv.validate(new UserInput(user.getNom(), user.getPrenom(),user.getEmail(),user.getRole(),
            		user.getStatut(), user.getPays()));
            user.setId(userId);
            User result = ur.save(user);
            return ResponseEntity.ok(userToResource(result, true));
        }
        return ResponseEntity.status(404).header("header", "user-invalid").body("L'utilisateur est introuvable ou supprimé !");
    }
    
    //DELETE
  	@DeleteMapping(value="/{userId}")
  	@Transactional
  	public ResponseEntity<?> deleteUser(@PathVariable("userId") String userId){
  		//Check if ID exist
  		Optional<User> userToDel = ur.findById(userId);
  		if (!userToDel.isPresent()) {
  			return ResponseEntity.status(404).header("header", "user-invalid").body("L'utilisateur est introuvable ou supprimé !");
  		}
          User user = userToDel.get();
          user.setStatut("SUPPRIMÉ");
          User result = ur.save(user);
          return ResponseEntity.ok(userDelToResource(result));
  	}
  	
  	//////////////////////////////////////// HATOES PART /////////////////////////////////////////////////////////
  	
  	//Static methods 
  	
  	private static Link selfLink(User user) {
  		return  linkTo(methodOn(UserRepresentation.class).getUser(user.getId())).withSelfRel();
  	}
  	
  	private static Link usersListLink() {
  		return linkTo(methodOn(UserRepresentation.class).getAllUsers()).withRel("Liste des utilisateurs");
  	}
  	
  	private static Link searchUserLink() {
  		return linkTo(methodOn(UserRepresentation.class).searchUser(null, null, null)).withRel("Rechercher un utilisateur");
  	}
  	
  	private static Link createNewUserLink() {
  		return linkTo(methodOn(UserRepresentation.class).registerUser(null)).withRel("Ajouter un nouvel utilisateur");
  	}
  	
  	private static Link updateUserLink(User user) {
  		return linkTo(methodOn(UserRepresentation.class).updateUser(user.getId(), null)).withRel("Modifier un utilisateur (PUT/PATCH)");
  	}
  	
  	private static Link deleteUserLink(User user) {
  		return linkTo(methodOn(UserRepresentation.class).deleteUser(user.getId())).withRel("Supprimer un utilisateur (DELETE)");
  	}
  	
  	
  	 private EntityModel<User> userToResource(User user) {
         Link usersLink = linkTo(methodOn(UserRepresentation.class).getAllUsers()).withRel("Gestion des utilisateurs");
         Link coursesLink = linkTo(methodOn(CourseRepresentation.class).getAllCourses()).withRel("Gestion des cours & épisodes");
         return EntityModel.of(user, selfLink(user), usersLink, coursesLink);
     }
  	
	private CollectionModel<EntityModel<User>> userToResource(Iterable<User> users) {
        Link selfLink = linkTo(methodOn(UserRepresentation.class).getAllUsers()).withSelfRel();
        //link to POST method
        List<EntityModel<User>> userResources = new ArrayList();
        users.forEach(user -> userResources.add(userToResource(user, false)));
        return  CollectionModel.of(userResources, usersListLink(), createNewUserLink(), searchUserLink());
    }

    private EntityModel<User> userToResource(User user, Boolean collection) {
        if (Boolean.TRUE.equals(collection)) {
            return EntityModel.of(user, selfLink(user), updateUserLink(user), deleteUserLink(user), usersListLink());
        } else {
            return EntityModel.of(user, selfLink(user));
        }
    }
    
    private EntityModel<User> userDelToResource(User user) {
            return EntityModel.of(user, selfLink(user), usersListLink());
    }

}
