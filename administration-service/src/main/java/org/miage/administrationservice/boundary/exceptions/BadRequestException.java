package org.miage.administrationservice.boundary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = " Saisie incorrecte, veuillez renseigner les champs correspondants !")
public class BadRequestException extends RuntimeException {

}