package org.miage.administrationservice.boundary.courses;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.miage.administrationservice.boundary.exceptions.BadRequestException;
import org.miage.administrationservice.boundary.exceptions.CourseExistsException;
import org.miage.administrationservice.boundary.exceptions.EpisodeExistsException;
import org.miage.administrationservice.boundary.users.UserRepresentation;
import org.miage.administrationservice.entity.courses.*;
import org.miage.administrationservice.entity.episodes.Episode;
import org.miage.administrationservice.entity.episodes.EpisodeInput;
import org.miage.administrationservice.entity.episodes.EpisodeValidator;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.util.ReflectionUtils;

@RestController
// not need to define @RequestBody, restcontroller specific versionof controller
// RequestMapping, allow to define URI (value), and the result produces
@RequestMapping(value="/cours", produces= MediaType.APPLICATION_JSON_VALUE)
// HATEOAS create links based on model user
@ExposesResourceFor(Course.class)
@RequiredArgsConstructor
public class CourseRepresentation {
	
	private final CourseRessource cr;
	// add validator
	private final CourseValidator cv;
	
	private final EpisodeRessource epr;
	// add validator
	private final EpisodeValidator epv;
	
	//ResponseEntity : allow to set header, status, body... use to configure response
	
	//#################### COURSE PART ####################
	
	// GET all 
	@GetMapping
	public ResponseEntity<?> getAllCourses(){
		// like for each, iterate all element of the Iterable
		Iterable<Course> all = cr.findAll();
		// on retourne tout le http avec le header.. et dans le body on met toute la liste
		//return ResponseEntity.ok(all);
		return ResponseEntity.ok(courseToResource(all));
	}

	// GET one 
	@GetMapping(value="/{courseId}")
	public ResponseEntity<?> getCourse(@PathVariable("courseId") String id){
		return Optional.ofNullable(cr.findById(id)).filter(Optional::isPresent)
				//.map(i -> ResponseEntity.ok(i))
				.map(i -> ResponseEntity.ok(courseToResource(i.get(), true)))
				.orElse(ResponseEntity.notFound().build());
	}
	
	// GET by filters 
	@GetMapping(value="/")
	public ResponseEntity<?> searchCourse(@RequestParam(value="statut", required=false) List<String> statut,
			@RequestParam(value="nom", required=false) String nom, 
			@RequestParam(value="theme", required=false) String theme, @RequestParam(value="prix", required=false) Double prix){
		
		Iterable<Course> all;
		
		if(nom != null && theme != null && statut != null && prix != null) {
			all = cr.findAllByNomAndThemeAndPrixAndStatutIn(nom, theme, prix, statut);
		} else if(nom != null && theme != null && statut != null) {
			all = cr.findAllByNomAndThemeAndStatutIn(nom, theme, statut);
		} else if(nom != null && theme != null && prix != null) {
			all = cr.findByNomAndThemeAndPrix(nom, theme, prix);
		} else if(nom != null && theme != null) {
			all = cr.findByNomAndTheme(nom, theme);
		} else if(statut != null && nom != null) {
			all = cr.findAllByNomAndStatutIn(nom, statut);
		} else if(statut != null && theme != null) {
			all = cr.findAllByThemeAndStatutIn(theme, statut);
		} else if(statut != null) {
			all = cr.findAllByStatutIn(statut);
		} else {
			all = cr.findByNomOrTheme(nom,theme);
		}
		
		return ResponseEntity.ok(courseToResource(all));
	}
	
	// POST 
	@PostMapping
	@Transactional
	public ResponseEntity<?> saveCourse(@RequestBody @Valid CourseInput course) throws BadRequestException{
		Course c = new Course(UUID.randomUUID().toString(), course.getNom(), course.getTheme(),
				course.getDescription(), course.getStatut(), course.getPrix(), course.getNote());
		Course saved = cr.save(c);
		// slash allow to add sub resource in URI
		URI location = linkTo(CourseRepresentation.class).slash(saved.getId()).toUri();
		//return ResponseEntity.created(location).build();
		return ResponseEntity.status(201).header("location", location.toString()).body(courseToResource(saved, true));
	}
	
	//PUT
	@PutMapping(value="/{courseId}")
	@Transactional
	public ResponseEntity<?> updateCourse(@PathVariable("courseId") String courseId, @RequestBody @Valid Course courseUp) 
			throws BadRequestException{
		Optional<Course> body = Optional.ofNullable(courseUp);
		if (!body.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		if (!cr.existsById(courseId) || courseUp.getStatut().equals("Supprimé")) {
			return ResponseEntity
				      .status(404)
				      .header("header", "course-invalid")
				      .body("Le cours est indisponible !");
		}
		courseUp.setId(courseId);
		Course result = cr.save(courseUp);
		//return ResponseEntity.ok().build();
		return ResponseEntity.ok(courseToResource(result, true));
	}
	
	// PATCH
    @PatchMapping(value = "/{courseId}")
    @Transactional
    public ResponseEntity<?> updateCoursePartiel(@PathVariable("courseId") String courseId,
            @RequestBody @Valid Map<Object, Object> fields) {
        Optional<Course> body = cr.findById(courseId);
        if (body.isPresent()) {
            Course course = body.get();
            fields.forEach((f, v) -> {
                Field field = ReflectionUtils.findField(Course.class, f.toString());
                field.setAccessible(true);
                ReflectionUtils.setField(field, course, v);
            });
            if (!cr.existsById(courseId) || course.getStatut().equals("Supprimé")) {
            	return ResponseEntity
          		      .status(404)
          		      .header("header", "course-invalid")
				      .body("Le cours est indisponible !");
    		}
            cv.validate(new CourseInput(course.getNom(), course.getTheme(),course.getDescription(), 
            		course.getStatut(), course.getPrix(), course.getNote()));
            course.setId(courseId);
            Course result = cr.save(course);
            return ResponseEntity.ok(courseToResource(result, true));
        }
        return ResponseEntity.status(404).header("header", "course-invalid").body("Le cours est indisponible !");
    }
    
    //DELETE
  	@DeleteMapping(value="/{courseId}")
  	@Transactional
  	public ResponseEntity<?> deleteCourse(@PathVariable("courseId") String courseId){
  		//Check if ID exist
  		Optional<Course> courseToDel = cr.findById(courseId);
  		if (!courseToDel.isPresent()) {
  			return ResponseEntity.status(404).header("header", "course-invalid").body("Le cours est indisponible !");
  		}
          Course course = courseToDel.get();
          course.setStatut("Supprimé");
          for (Episode episode : course.getEpisodes()) {
        	  episode.setStatut("Supprimé");
          }
          Course result = cr.save(course);
          return ResponseEntity.ok(courseDelToResource(result));
  	}
    
    //////////////////////////////////////////// EPISODE PART /////////////////////////////////////////////////////////////////
    
  	// GET all episodes of a course
 	@GetMapping(value="/{courseId}/episodes")
 	public ResponseEntity<?> getAllEpisodesCourse(@PathVariable("courseId") String courseId){
 		// like for each, iterate all element of the Iterable
 		Iterable<Episode> all = epr.findByCourseId(courseId);
 		// on retourne tout le http avec le header.. et dans le body on met toute la liste
 		return ResponseEntity.ok(episodeToResource(all, courseId));
 	}
 	
 	// GET one 
 	@GetMapping(value="/{courseId}/episodes/{episodeId}")
 	public ResponseEntity<?> getEpisode(@PathVariable("courseId") String courseId, @PathVariable("episodeId") String id){
 		return Optional.ofNullable(epr.findByCourseIdAndId(courseId, id)).filter(Optional::isPresent)
 				.map(i -> ResponseEntity.ok(episodeToResource(courseId, i.get(), true)))
 				.orElse(ResponseEntity.notFound().build());
 	}
 	
 	// POST
 	@PostMapping(value="/{courseId}/episodes")
 	@Transactional
 	public ResponseEntity<?> saveEpisode(@PathVariable("courseId") String courseId, @RequestBody @Valid EpisodeInput episode)
 			throws BadRequestException {
 		Episode e = new Episode(UUID.randomUUID().toString(), episode.getTitre(), episode.getUrl(), 
 				episode.getStatut(), episode.getCourse());
 		Episode saved = epr.save(e);
 		episode.getCourse().ajouterEpisodes(e);
 		URI location = linkTo(CourseRepresentation.class).slash(saved.getId()).toUri();
 		// slash allow to add sub resource in URI
 		return ResponseEntity.status(201).header("location", location.toString()).body(episodeToResource(courseId, saved, true));
 	}
 	
 	//PUT
 	@PutMapping(value="/{courseId}/episodes/{episodeId}")
 	@Transactional
 	public ResponseEntity<?> updateEpisode(@PathVariable("courseId") String courseId, @PathVariable("episodeId") String episodeId, 
 			@RequestBody @Valid Episode episodeUp){
 		Optional<Episode> body = Optional.ofNullable(episodeUp);
 		if (!body.isPresent() || episodeUp.getStatut().equals("Supprimé")) {
			return ResponseEntity
				      .status(404)
				      .header("header", "episode-invalid")
				      .body("L'épisode est introuvable ou supprimé !");
 		}
 		if (!epr.existsById(episodeId)) {
 			return ResponseEntity.status(404).header("header", "course-invalid").body("L'épisode n'existe pas ou est supprimé !");
 		}
 		Course course = cr.findById(courseId).get();
 		//Pour set l'id de l'épisode 
 		episodeUp.setId(episodeId);
 		episodeUp.setCourse(course);
 		System.out.println(episodeUp);
 		//Pour set le course de l'épisode
 		Episode result = epr.save(episodeUp);
 		return ResponseEntity.ok(episodeToResource(courseId, result, true));
 	}
 	
 	// PATCH
     @PatchMapping(value = "/{courseId}/episodes/{episodeId}")
     @Transactional
     public ResponseEntity<?> updateEpisodePartiel(@PathVariable("courseId") String courseId, @PathVariable("episodeId") String episodeId,
             @RequestBody @Valid Map<Object, Object> fields) {
         Optional<Episode> body = epr.findById(episodeId);
         if (body.isPresent()) {
             Episode episode = body.get();
             fields.forEach((f, v) -> {
                 Field field = ReflectionUtils.findField(Episode.class, f.toString());
                 field.setAccessible(true);
                 ReflectionUtils.setField(field, episode, v);
             });
             if (!epr.existsById(episodeId) || episode.getStatut().equals("Supprimé")) {
            	 return ResponseEntity
	   				      .status(404)
	   				      .header("header", "episode-invalid")
	   				      .body("L'épisode est introuvable ou supprimé !");
     		}
             epv.validate(new EpisodeInput(episode.getTitre(),episode.getUrl(), episode.getStatut(), episode.getCourse()));
             episode.setId(episodeId);
             Episode result = epr.save(episode);
             return ResponseEntity.ok(episodeToResource(courseId, result, true));
         }
		return ResponseEntity.status(404).header("header", "course-invalid").body("L'épisode n'existe pas ou est supprimé !");
     }
     
   //DELETE
  	@DeleteMapping(value="/{courseId}/episodes/{episodeId}")
  	@Transactional
  	public ResponseEntity<?> deleteEpisode(@PathVariable("courseId") String courseId, @PathVariable("episodeId") String episodeId){
  		//Check if ID exist
  		Optional<Episode> episodeToDel = epr.findById(episodeId);
  		if (!episodeToDel.isPresent()) {
 			return ResponseEntity.status(404).header("header", "course-invalid").body("L'épisode n'existe pas ou est supprimé !");
 		}
  		Episode episode = episodeToDel.get();
  		episode.setStatut("Supprimé");
        epr.save(episode);
          
        return ResponseEntity.ok(episodeDelToResource(courseId, episode));
  	}
 	
 	/*
 	// GET by filters 
 	@GetMapping(value="/")
 	public ResponseEntity<?> searchCourse(@RequestParam(value="statut", required=false) List<String> statut,
 			@RequestParam(value="nom", required=false) String nom, 
 			@RequestParam(value="prenom", required=false) String prenom){
 		
 		Iterable<User> all = ur.findByNomOrPrenom(nom,prenom);
 		
 		if(nom != null && prenom != null)
 			all = ur.findByNomAndPrenom(nom, prenom);
 		
 		if(statut != null)
 			all = ur.findAllByStatutIn(statut);
 		
 		if(statut != null && nom != null)
 			all = ur.findAllByNomAndStatutIn(nom, statut);
 		
 		return ResponseEntity.ok(userToResource(all));
 	}*/
    
    //#################### RESSOURCE PART ####################
  	
  	//Static methods 
  	
  	private static Link selfLink(Course course) {
  		return  linkTo(methodOn(CourseRepresentation.class).getCourse(course.getId())).withSelfRel();
  	}
  	
  	private static Link coursesListLink() {
  		return linkTo(methodOn(CourseRepresentation.class).getAllCourses()).withRel("Liste des cours");
  	}
  	
  	private static Link episodesCourseListLink(Course course) {
  		return linkTo(methodOn(CourseRepresentation.class).getAllEpisodesCourse(course.getId())).withRel("Liste des épisodes");
  	}
  	
  	private static Link searchCourseLink() {
  		return linkTo(methodOn(CourseRepresentation.class).searchCourse(null, null, null, null)).withRel("Rechercher un cours");
  	}
  	
  	private static Link createNewCourseLink() {
  		return linkTo(methodOn(CourseRepresentation.class).saveCourse(null)).withRel("Ajouter un nouveau cours");
  	}
  	
  	private static Link updateCourseLink(Course course) {
  		return linkTo(methodOn(CourseRepresentation.class).updateCourse(course.getId(), null)).withRel("Modifier un cours (PUT/PATCH)");
  	}
  	
  	private static Link deleteCourseLink(Course course)  {
  		return linkTo(methodOn(CourseRepresentation.class).deleteCourse(course.getId())).withRel("Supprimer un cours (DELETE)");
  	}
	
	private CollectionModel<EntityModel<Course>>courseToResource(Iterable<Course> courses) {
        List<EntityModel<Course>> courseResources = new ArrayList<EntityModel<Course>>();
        courses.forEach(course -> courseResources.add(courseToResource(course, false)));
        return  CollectionModel.of(courseResources, coursesListLink(), createNewCourseLink(), searchCourseLink());
    }

    private EntityModel<Course> courseToResource(Course course, Boolean collection) { 
        if (Boolean.TRUE.equals(collection)) {
            return EntityModel.of(course, selfLink(course), episodesCourseListLink(course), updateCourseLink(course), deleteCourseLink(course), coursesListLink());
        } else {
            return EntityModel.of(course, selfLink(course), episodesCourseListLink(course));
        }
    }
    
    private EntityModel<Course> courseDelToResource(Course course) {
        return EntityModel.of(course, selfLink(course), episodesCourseListLink(course), coursesListLink());
    }
    
    ////////////////////////////////////  HATOES EPISODE PART //////////////////////////////////////////////////////////////////
    
    //Static methods 
    
    private static Link selfLink(String courseId, Episode episode) {
  		return linkTo(CourseRepresentation.class)
         		.slash(courseId)
         		.slash("episodes")
         		.slash(episode.getId())
         		.withSelfRel();
  	}
    
    private static Link courseEpisodesListLink(String courseId) {
  		return linkTo(methodOn(CourseRepresentation.class).getAllEpisodesCourse(courseId)).withRel("Liste des épisodes");
  	}
  	
  	private static Link courseLink(String courseId) {
  		return linkTo(methodOn(CourseRepresentation.class).getCourse(courseId)).withRel("Cours");
  	}
  	
  	private static Link createNewEpisodeLink(String courseId) {
  		return linkTo(methodOn(CourseRepresentation.class).saveEpisode(courseId, null)).withRel("Ajouter un nouvel épisode");
  	}
  	
  	private static Link updateEpisodeLink(String courseId, Episode episode) {
  		return linkTo(methodOn(CourseRepresentation.class).updateEpisode(courseId, episode.getId(), null)).withRel("Modifier un épisode (PUT/PATCH)");
  	}
  	
  	private static Link deleteEpisodeLink(String courseId, Episode episode)  {
  		return linkTo(methodOn(CourseRepresentation.class).deleteEpisode(courseId, episode.getId())).withRel("Supprimer un épisode (DELETE)");
  	}
    
    private CollectionModel<EntityModel<Episode>>episodeToResource(Iterable<Episode> episodes, String courseId) {
        List<EntityModel<Episode>> episodesResources = new ArrayList<EntityModel<Episode>>();
        episodes.forEach(episode -> episodesResources.add(episodeToResource(courseId, episode, false)));
        return CollectionModel.of(episodesResources, courseEpisodesListLink(courseId), createNewEpisodeLink(courseId), courseLink(courseId));
     }

     private EntityModel<Episode> episodeToResource(String courseId, Episode episode, Boolean collection) {
         if (Boolean.TRUE.equals(collection)) {
             return EntityModel.of(episode, selfLink(courseId,episode), 
            		 updateEpisodeLink(courseId, episode),
            		 deleteEpisodeLink(courseId, episode),
            		 courseEpisodesListLink(courseId));
         } else {
             return EntityModel.of(episode, selfLink(courseId,episode));
         }
     }
     
     private EntityModel<Episode> episodeDelToResource(String courseId, Episode episode) {
         return EntityModel.of(episode, selfLink(courseId, episode), courseEpisodesListLink(courseId));
     }
}
