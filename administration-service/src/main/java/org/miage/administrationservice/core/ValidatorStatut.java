package org.miage.administrationservice.core;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidatorStatut implements ConstraintValidator<ValidateStatus, String> { 
	
	private ValidateStatus annotation;
	 
    @Override
    public void initialize(ValidateStatus annotation)
    {
        this.annotation = annotation;
    }
 
    @Override
    public boolean isValid(String valueForValidation, ConstraintValidatorContext constraintValidatorContext)
    {
        boolean result = false;
         
        Object[] enumValues = this.annotation.enumClass().getEnumConstants();
         
        if(enumValues != null)
        {
            for(Object enumValue:enumValues)
            {
                if(valueForValidation.equals(enumValue.toString()))
                {
                    result = true; 
                    break;
                }
            }
        }
         
        return result;
    }
} 
