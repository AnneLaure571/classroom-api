package org.miage.administrationservice.core;

public enum UserStatusEnum {
	
	NOUVEAU,
	BON_ELEVE,
	MAUVAIS_PAYEUR,
	SUPPRIMÉ
}

