package org.miage.administrationservice.core;

public enum RoleTypeEnum {
	
	ADMIN,
	PROFESSOR,
	STUDENT,
	USER;

}
