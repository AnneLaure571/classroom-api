package org.miage.administrationservice.entity.episodes;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.miage.administrationservice.entity.courses.Course;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor //Attention, INDISPENSABLE pour JPA !
@AllArgsConstructor
public class Episode implements Serializable{
	
	private static final long serialVersionUID = 1234567854567L;
	
	@Id
	//TODO remove this for user UID
	//@GeneratedValue(strategy= GenerationType.IDENTITY)
	private String id;
	private String titre;
	private String url;
	private String statut;
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY )
	@JoinColumn(name = "course_id", referencedColumnName="id")
	//@JsonManagedReference
    private Course course;

}