package org.miage.administrationservice.entity.users;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.miage.administrationservice.core.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInput {
	
    @NotNull
    @NotBlank
    private String nom;
    @Size(min=2)
    private String prenom;
    @NotNull
    @NotBlank
    private String email;
    @NotNull
    @ValidateRole(enumClass=RoleTypeEnum.class) 
    private String role;
    @NotNull
    @NotBlank
    @ValidateStatus(enumClass=UserStatusEnum.class) 
    @Size(min=5)
    private String statut;
    @Size(min=3)
    private String pays;
	
}
