package org.miage.administrationservice.entity.episodes;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.stereotype.Service;

@Service
public class EpisodeValidator {
	
	private Validator validator;

	EpisodeValidator(Validator validator) {
		this.validator = validator;
	}
	
	public void validate(EpisodeInput course) {
		Set<ConstraintViolation<EpisodeInput>> violations = validator.validate(course);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
	}

}
