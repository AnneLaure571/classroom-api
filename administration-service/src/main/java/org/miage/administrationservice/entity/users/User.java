package org.miage.administrationservice.entity.users;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
/* permet de générer automatiquement les getters et setters*/
@NoArgsConstructor //Attention, INDISPENSABLE pour JPA !
@AllArgsConstructor
public class User implements Serializable{
	
	private static final long serialVersionUID = 1234567854567L;
	
	@Id
	//TODO remove this for user UID
	//@GeneratedValue(strategy= GenerationType.IDENTITY)
	private String id;
	private String nom;
	private String prenom;
	private String email;
	private String role;
	private String statut;
	private String pays;

}
