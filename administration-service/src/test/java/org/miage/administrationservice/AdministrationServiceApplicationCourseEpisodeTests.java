package org.miage.administrationservice;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.miage.administrationservice.boundary.courses.CourseRessource;
import org.miage.administrationservice.boundary.courses.EpisodeRessource;
import org.miage.administrationservice.entity.courses.Course;
import org.miage.administrationservice.entity.episodes.Episode;
import org.springframework.boot.test.context.SpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AdministrationServiceApplicationCourseEpisodeTests {
	
	@LocalServerPort
    int port;
	
	@Autowired
    CourseRessource cr;
	
	@Autowired
    EpisodeRessource epr;
	
	 @BeforeEach
    public void setupContext() {
		epr.deleteAll();
		cr.deleteAll();
        RestAssured.port = port;
    }
	
	//////////////////////////////////////////////// Tests COURSE ////////////////////////////////////////////////////////////
	 
    @Test
    public void pingCourse() {
        when().get("/cours").then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void getAllCourses() {
        Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Java", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        Course c2 = new Course(UUID.randomUUID().toString(), "Programmation Python", "Programmation", "cours débutants/avancés", "Disponible", 10.0, 4.0);
        cr.save(c2);
        when().get("/cours").then().statusCode(HttpStatus.SC_OK)
                .and().assertThat().body("size()", equalTo(2));
    }

    @Test
    public void getOneCourse() {
    	Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Go", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        Response response = when().get("/cours/" + c1.getId())
                .then().statusCode(HttpStatus.SC_OK).extract().response();
        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString("Go"));
    }

    @Test
    public void postCourse() throws Exception {
    	Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Go", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        Response response = given().body(this.toJsonString(c1)).contentType(ContentType.JSON).when()
                .post("/cours").then().statusCode(HttpStatus.SC_CREATED).extract().response();
        String location = response.getHeader("Location");
        when().get(location).then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void putCourse() throws Exception {
    	Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Go", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        c1.setNote(4.0);
        c1.setPrix(10.0);;

        given().body(this.toJsonString(c1)).contentType(ContentType.JSON).when().put("/cours/" + c1.getId())
                .then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void deleteCourse() throws Exception {
    	Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Go", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        when().delete("/cours/" + c1.getId()).then().statusCode(HttpStatus.SC_OK);
    }
    
    /////////////////////////////////////////////////Tests EPISODE ////////////////////////////////////////////////////////////
    @Test
    public void pingEpisodes() {
    	Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Go", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        when().get("/cours/" + c1.getId() + "/episodes").then().statusCode(HttpStatus.SC_OK);
    }

   @Test
    public void getAllEpisodes() {
        Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Java", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        Episode e1 = new Episode(UUID.randomUUID().toString(),"Introduction", "rien", "Disponible", c1);
        epr.save(e1);
        Episode e2 = new Episode(UUID.randomUUID().toString(),"Partie 1", "rien", "Disponible", c1);
        epr.save(e2);
        when().get("/cours/" + c1.getId() + "/episodes").then().statusCode(HttpStatus.SC_OK)
                .and().assertThat().body("size()", equalTo(2));
    }
   
   @Test
   public void getOneEpisode() {
	   Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Java", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
       cr.save(c1);
       Episode e1 = new Episode(UUID.randomUUID().toString(),"Introduction", "rien", "Disponible", c1);
       epr.save(e1);
       Response response = when().get("/cours/" + c1.getId() + "/episodes/" + e1.getId())
               .then().statusCode(HttpStatus.SC_OK).extract().response();
       String jsonAsString = response.asString();
       assertThat(jsonAsString, containsString("Introduction"));
   }
   
   @Test
   public void postEpisode() throws Exception {
	   Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Java", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
       cr.save(c1);
       Episode e1 = new Episode(UUID.randomUUID().toString(),"Introduction", "rien", "Disponible", c1);
       epr.save(e1);
       Response response = when().get("/cours/" + c1.getId() + "/episodes/" + e1.getId())
               .then().statusCode(HttpStatus.SC_OK).extract().response();
   }

    @Test
    public void putEpisode() throws Exception {
    	Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Go", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        Episode e1 = new Episode(UUID.randomUUID().toString(),"Introduction", "rien", "Disponible", c1);
        epr.save(e1);
        e1.setStatut("Supprimé");
        e1.setUrl("blabla");
        given().body(this.toJsonString(c1)).contentType(ContentType.JSON).when().put("/cours/" + c1.getId() + "/episodes/" + e1.getId())
                .then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void deleteEpisode() throws Exception {
    	Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Go", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
        cr.save(c1);
        Episode e1 = new Episode(UUID.randomUUID().toString(),"Introduction", "rien", "Disponible", c1);
        epr.save(e1);
        when().delete("/cours/" + c1.getId() + "/episodes/" + e1.getId()).then().statusCode(HttpStatus.SC_OK);
    }
    
    private String toJsonString(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(r);
    }

}
