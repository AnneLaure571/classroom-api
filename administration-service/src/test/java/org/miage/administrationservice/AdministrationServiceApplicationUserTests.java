package org.miage.administrationservice;

import org.junit.jupiter.api.Test;
import org.miage.administrationservice.boundary.courses.CourseRessource;
import org.miage.administrationservice.boundary.users.UserRessource;
import org.miage.administrationservice.entity.courses.Course;
import org.miage.administrationservice.entity.users.User;
import org.springframework.boot.test.context.SpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.UUID;
import org.apache.http.HttpStatus;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AdministrationServiceApplicationUserTests {

	 	@LocalServerPort
	    int port;

	    @Autowired
	    UserRessource ur;

	    @BeforeEach
	    public void setupContext() {
	        ur.deleteAll();
	        RestAssured.port = port;
	    }

		@Test
	    public void pingUser() {
	        when().get("/utilisateurs").then().statusCode(HttpStatus.SC_OK);
	    }

	    @Test
	    public void getAllUser() {
	        User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER", "NOUVEAU", "Nancy");
	        ur.save(u1);
	        User i2 = new User(UUID.randomUUID().toString(), "Blanc", "Robert", "blanc-robert@gmail.com", "ADMIN", "NOUVEAU", "Nancy");
	        ur.save(i2);
	        when().get("/utilisateurs").then().statusCode(HttpStatus.SC_OK)
	                .and().assertThat().body("size()", equalTo(2));
	    }

	    @Test
	    public void getOneUser() {
	    	User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER", "NOUVEAU", "Nancy");
	        ur.save(u1);
	        Response response = when().get("/utilisateurs/" + u1.getId())
	                .then().statusCode(HttpStatus.SC_OK).extract().response();
	        String jsonAsString = response.asString();
	        assertThat(jsonAsString, containsString("Tom"));
	    }

	    @Test
	    public void postUser() throws Exception {
	    	User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER", "NOUVEAU", "Nancy");
	        Response response = given().body(this.toJsonString(u1)).contentType(ContentType.JSON).when()
	                .post("/utilisateurs").then().statusCode(HttpStatus.SC_CREATED).extract().response();
	        String location = response.getHeader("Location");
	        when().get(location).then().statusCode(HttpStatus.SC_OK);
	    }

	    @Test
	    public void putUser() throws Exception {
	    	User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER", "NOUVEAU", "Nancy");
	        ur.save(u1);
	        u1.setPays("France");
	        u1.setRole("ADMIN");;

	        given().body(this.toJsonString(u1)).contentType(ContentType.JSON).when().put("/utilisateurs/" + u1.getId())
	                .then().statusCode(HttpStatus.SC_OK);
	        /*given().headers("Accept", ContentType.JSON).when().get("/utilisateurs/" + u1.getId()).then()
	                .statusCode(HttpStatus.SC_OK)
	                .and().assertThat().body("role", equalTo(u1.getRole()));*/
	    }

	    @Test
	    public void deleteUser() throws Exception {
	    	User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER", "NOUVEAU", "Nancy");
	        ur.save(u1);
	        when().delete("/utilisateurs/" + u1.getId()).then().statusCode(HttpStatus.SC_OK);
	    }

	    private String toJsonString(Object r) throws Exception {
	        ObjectMapper map = new ObjectMapper();
	        return map.writeValueAsString(r);
	    }

}
