# Administration-service

## Architecture

Le service est divisé en deux parties

* la partie gérant les utilisateurs :
    * UserRepresentation : controller de l'api avec les endpoints, les méthodes;
    * UserResource : repository (ou interface) jpa permettant l'utilisation des méthodes.
* la partie gérant les cours :
    * CourseRepresentation : controller de l'api avec les endpoints, les méthodes;
    * CourseResource : repository (ou interface) jpa permettant l'utilisation des méthodes;
    * EpisodeResource : repository (ou interface) jpa permettant l'utilisation des méthodes.

## Démarrer le service

### Générer le .jar

Pour passer les tests unitaires :

```
mvn -DskipTests clean install
```

sinon 

```
mvn clean install
```

### Tester

`port : 8084`

```
java -jar administration-service.jar
```

## Accéder au service

* Pour importer les routes du service administration sur Postman en local, aller sur l'URL [Open API Admin via Postman](http://localhost:8084/v3/api-docs/)

`OU` 

Y accéder via la collection en ligne :

[![cliquez ici](https://run.pstmn.io/button.svg)](https://www.getpostman.com/collections/36823de5c546980d9388) 

`OU`

* [Open API Admin via Swagger](http://localhost:8084/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#)

Ce service comporte toutes les fonctionnalités auxquelles un utilisateur a accès sur l'API de cours en ligne. Toutes informations concernant la conception (entity, boundary etc...) sont détaillées dans le rapport au format `.docx` ou `.pdf`.

## DockerFile

Il est possible de lancer à l'aide de `Docker`, le service administration via le Dockerfile.


## Liste des fonctionnalités des utilisateurs

### Se connecter

`Méthode : login, Type : GET`

Cette méthode sert de "pseudo" connexion, en utilisant l'email de l'usager qui est son identifiant pour accéder à la plateforme.

* URL : `https://localhost:8084/utilisateurs/login?email=` ou `{{baseUrl}}/utilisateurs/login?email=`

Exemple :

```
ADMIN :
{{baseUrl}}/utilisateurs/login/?email=albert-ingals@gmail.com

USER :
{{baseUrl}}/utilisateurs/login?email=alicia-ingals@gmail.com

```

Avec HATOES, il y a une méthode spécifique pour la méthode `login` car, par rapport aux autres méthodes `userToResource`, celle-ci est juste utilisé pour afficher les liens qui permettent la gestion de l'API (étant donné que ce service est réservé aux administrateurs). On vérifie que l'utilisateur qui tente de s'identifier possède bien les droits utilisateurs, auquel cas, on retourne une badRequest avec pour message, que l'utilisateur ne dispose pas des accès suffisants.

Il y a plusieurs liens :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs,
* un pour ajouter un nouvel utilisateur,
* un pour la recherche (avec les paramètres).

```
"self": {
     "href": "http://localhost:8084/utilisateurs/c0b7a868-5372-11eb-ae93-0242ac130002"
}
.....
""_links": {
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    },
    "Ajouter un nouvel utilisateur": {
        "href": "http://localhost:8084/utilisateurs"
    },
    "Rechercher un utilisateur": {
        "href": "http://localhost:8084/utilisateurs/{?statut,nom,prenom}",
        "templated": true
    }
}
```

Le lien renvoie vers le profil de l'utilisateur, ce qui permet d'accéder directement au profil de l'utilisateur selectionné (`status OK`). Si l'utilisateur ne possède pas les droits nécessaires, il y aura une réponse `status UNAUTHORIZED` ou une réponse `status BAD_REQUEST` si l'email ne retourne aucun utilisateur.

### Lister tous les utilisateurs

`Méthode : getAllUsers, Type : GET`

Cette méthode permet de lister l'ensemble des utilsiateurs du site.

* URL : `https://localhost:8084/utilisateurs` ou `{{baseUrl}}/utilisateurs`

Avec HATOES, il y a quatre liens qui ont généré :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs,
* un pour modifier le cours,
* un pour supprimer le cours.

```
"self": {
     "href": "http://localhost:8084/utilisateurs/c0b7a868-5372-11eb-ae93-0242ac130002"
}
.....
""_links": {
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    },
    "Ajouter un nouvel utilisateur": {
        "href": "http://localhost:8084/utilisateurs"
    },
    "Rechercher un utilisateur": {
        "href": "http://localhost:8084/utilisateurs/{?statut,nom,prenom}",
        "templated": true
    }
}
```

La méthode renvoie la liste des utilisateurs (`status OK`), ce qui permet d'accéder directement au profil de l'utilisateur selectionné. Si l'utilisateur n'est pas trouvé, 
on retourne une réponse HTTP NOT_FOUND (`status NOT_FOUND`).

### Accéder aux ressources d'un utilisateur

`Méthode : getUser, Type : GET`

Cette méthode permet d'accéder au profil d'un utilisateur.

* URL : `https://localhost:8084/utilisateurs/:userId` ou `{{baseUrl}}/utilisateurs/:userId`

Avec HATOES, il y a quatre liens qui ont été généré :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs,
* un pour modifier le cours,
* un pour supprimer le cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Modifier un utilisateur (PUT/PATCH)": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un utilisateur (DELETE)": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    }
}
```

La méthode renvoie le profil de l'utilisateur (`status OK`), ce qui permet d'accéder directement au profil de l'utilisateur selectionné. À l'inverse dans le service user, 
on a en plus la liste des cours suivis, la liste des épisodes visionnés. S'il n'y a pas d'utilisateur correspondant à cet id, on retourne une réponse `status NOT_FOUND`.

### Rechercher des utilisateurs

`Méthode : searchUser, Type : GET`

Cette méthode permet de rechercher un ou plusieurs utilisateurs à partir des champs suivants :

* statut : nécessite un paramètre de type `String`, ex : mauvais payeur,
* nom : nécessite un paramètre de type `String`, ex : Ingals,
* prenom : nécessite un paramètre de type `String`, ex : Albert.

Si vous souhaitez tester avec plusieurs statuts, il faut le faire de la manière suivante :

* {{baseUrl}}/utilisateurs/?statut=NOUVEAU&statut=MAUVAIS_PAYEUR

Les statuts sont définis dans un enum, hors pour la recherche, il est plus pratique de faire une recherche `statut=NOUVEAU` que `statut=0`, c'est pourquoi, il y a une énumération avec une liste prédéfinie (qui peut être complétée au fur et à mesure), j'ai crée une annotation Spring custom qui permet de vérifier si le statut inséré est bien dans l'enumération.

Dans le package core :
```
public enum UserStatusEnum {
	
	NOUVEAU,
	BON_ELEVE,
	MAUVAIS_PAYEUR,
	SUPPRIMÉ
}
```

Dans le package entity.users :

```
@ValidateStatus(enumClass=UserStatusEnum.class) 
@Size(min=5)
private String statut;
```

Dans le service utilisateurs (User-service), le statut est de type UserStatusEnum, car l'utilisateur qui s'inscrit est forcément un utilisateur, donc par besoin de passer par UserInput et le statut est défini dans la méthode subscribe et il n'y a pas de recherche pour les utilisateurs.

* URL : `https://localhost:8084/utilisateurs/?statut=NOUVEAU&statut=MAUVAIS_PAYEUR&nom=Ingals&prenom=Albert` ou `{{baseUrl}}/utilisateurs/?statut=NOUVEAU&statut=MAUVAIS_PAYEUR&nom=Ingals&prenom=Albert`

la liste des différentes recherches possibles :

* findAllByNomAndPrenomAndStatutIn(nom, prenom, statut);
* findByNomAndPrenom(nom, prenom);
* findAllByNomAndStatutIn(nom, statut);
* findAllByPrenomAndStatutIn(prenom, statut);
* findAllByStatutIn(statut);
* findByNomOrPrenom(nom,prenom).

Avec un ou plusieurs statut(s), un nom, un prénom.

Avec HATOES, il y a 4 liens qui ont été généré :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs,
* un pour modifier le cours,
* un pour supprimer le cours.

```
"_links": {
        "self": {
        "href": "http://localhost:8084/utilisateurs/c0b7a868-5372-11eb-ae93-0242ac130002"
        }
}
....
""_links": {
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    },
    "Ajouter un nouvel utilisateur": {
        "href": "http://localhost:8084/utilisateurs"
    },
    "Rechercher un utilisateur": {
        "href": "http://localhost:8084/utilisateurs/{?statut,nom,prenom}",
        "templated": true
    }
}
```

Le lien renvoie vers le profil de l'utilisateur ou les profils d'utilisateurs (`status OK`), ce qui permet d'accéder directement au profil de l'utilisateur selectionné.

### Ajouter un nouvel utilisteur (ADMIN, PROFESSOR etc)

`Méthode : registerUser, Type : POST`

* URL : `https://localhost:8084/utilisateurs` ou `{{baseUrl}}/utilisateurs`

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "email": "willy-olson@gmail.com",
    "nom": "Olson",
    "prenom": "Willy",
    "pays": "France",
    "role": "ADMIN",
    "statut": "NOUVEAU"
}
```

L'affichage est possible car on retourne dans la méthode registerUser, une réponse HTTP `201 created` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.status(201).header("location", location.toString()).body(userToResource(saved, true));
```

Avec HATOES, il y a quatre liens qui ont été généré :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs,
* un pour modifier le cours,
* un pour supprimer le cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/utilisateurs/ea4e6158-47a5-4b66-91ca-251c635b288c"
    },
    "Modifier un utilisateur (PUT/PATCH)": {
        "href": "http://localhost:8084/utilisateurs/ea4e6158-47a5-4b66-91ca-251c635b288c"
    },
    "Supprimer un utilisateur (DELETE)": {
        "href": "http://localhost:8084/utilisateurs/ea4e6158-47a5-4b66-91ca-251c635b288c"
    },
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    }
}
```

Si l'email existe déjà, on informera en retournant une erreur de conflit (`409 conflict`) avec pour message dans le body, "Un utilisateur existe déjà avec cet email !", sinon si les champs sont vides, un exception.

### Modifier un utilisatieur

`Méthode : updateUser, Type : PUT`

* URL : `https://localhost:8084/utilisateurs/:userId` ou `{{baseUrl}}/utilisateurs/:userId`

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "email": "willy-olson@gmail.com",
    "nom": "Olson",
    "prenom": "Willy",
    "pays": "France",
    "role": "USER",
    "statut": "NOUVEAU"
}
```

<span style="color:red">Note importante</span> : l'id est affiché dans le body JSON, mais il n'est pas nécessaire par contre, il est demandé en temps que variable paramètre dans l'endpoint. 

L'affichage est possible car on retourne dans la méthode updateUser, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES, l'utilisateur a bien été modifié.

```
ResponseEntity.ok(userToResource(result, true));
```

Avec HATOES, il y a quatre liens qui sont retournés :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs,
* un pour modifier le cours,
* un pour supprimer le cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Modifier un utilisateur (PUT/PATCH)": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un utilisateur (DELETE)": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    }
}
```

Si le body est vide ou que l'utilisateur n'exsite pas ou qu'il a été supprimé, il y aura une réponse badRequest `400 BAD_REQUEST` (body invalide) ou `404 NOT_FOUND` (pas d'utilisateur correspondant).

### Modifier un utilisatieur de manière partielle

`Méthode : updateUserPartiel, Type : PATCH`

* URL : `https://localhost:8084/utilisateurs/:userId` ou `{{baseUrl}}/utilisateurs/:userId`

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "email": "willy-olson@gmail.com",
    "nom": "Olson",
    "prenom": "Willy",
    "pays": "France",
    "role": "ADMIN",
    "statut": "MAUVAIS_PAYEUR"
}
```
On récupère le contenu du body (d'où l'utilisation de la Map), car il y peut y avoir un ou plusieurs champs.
L'affichage est possible car on retourne dans la méthode updateUserPartiel, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES, l'utilisateur a bien été modifié.

```
ResponseEntity.ok(userToResource(result, true));
```

Avec HATOES, il y a quatre liens qui sont retournés :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs,
* un pour modifier le cours,
* un pour supprimer le cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Modifier un utilisateur (PUT/PATCH)": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un utilisateur (DELETE)": {
        "href": "http://localhost:8084/utilisateurs/d7e4aafe-5372-11eb-ae93-0242ac130002"
    },
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    }
}
```
Si le body est vide ou que l'utilisateur n'exsite pas ou qu'il a été supprimé, il y aura une réponse badRequest  `400 BAD_REQUEST` (body invalide) ou `404 NOT_FOUND` (pas d'utilisateur correspondant).

### Supprimer un utilisateur

`Méthode : deleteUser, Type : DELETE`

* URL : `https://localhost:8084/utilisateurs/:userId` ou `{{baseUrl}}/utilisateurs/:userId`

L'affichage est possible car on retourne dans la méthode updateUserPartiel, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.ok(userToResource(result, true));
```

Avec HATOES, il y a deux liens qui sont retournés :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour accéder à la collection des utilisateurs.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/utilisateurs/123d5f65-4092-42d7-9f68-d5b32bcababf"
    },
    "Liste des utilisateurs": {
        "href": "http://localhost:8084/utilisateurs"
    }
}
```
Si le body est vide ou que l'utilisateur n'exsite pas ou qu'il a déjà été supprimé, il y aura une réponse badRequest  `400 BAD_REQUEST` (body invalide) ou `400 NOT_FOUND` (pas d'utilisateur correspondant).

## Liste des fonctionnalités cours & épisodes

### Lister tous les cours

`Méthode : getAllCourses, Type : GET`

Cette méthode permet de lister l'ensemble des cours du site.

* URL : `https://localhost:8084/cours` ou `{{baseUrl}}/cours`

Avec HATOES, il y a cinq liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour créer un nouveau cours,
* un pour réaliser une recherche avec les paramètres.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "episodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    }
}
...
"_links": {
    "Liste des cours": {
        "href": "http://localhost:8084/cours"
    },
    "Ajouter un nouveau cours": {
        "href": "http://localhost:8084/cours"
    },
    "Rechercher un cours": {
        "href": "http://localhost:8084/cours/{?statut,nom,theme,prix}",
        "templated": true
    }
}
```

On retourne la liste des cours, ce qui correspond à une réponse HTTP `200 OK`.

### Accéder aux ressources d'un cours en particulier

`Méthode : getCourse, Type : GET`

* URL : `https://localhost:8084/cours/:courseId` ou `{{baseUrl}}/cours/:courseId`

Avec HATOES, il y a cinq liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour modifier le cours,
* un pour supprimer le cours.

Exemple, vous pouvez essayer avec un ID fixe d'un cours (le cours est dans le script de peuplement de la base) -> `de5efccc-5372-11eb-ae93-0242ac130002`.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Modifier un cours (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un cours (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des cours": {
        "href": "http://localhost:8084/cours"
    }
}
```

S'il y a une correspondance avec l'id, on retourne les informations du cours (`status OK`) sinon on retourne une réponse `404 NOT_FOUND`.

### Rechercher des cours

`Méthode : searchCourse, Type : GET`

* URL : `https://localhost:8084/cours/?statut=Supprimé&statut=Disponible&nom=Docker&theme=DevOps&prix=10.0` ou `{{baseUrl}}/cours/?statut=Supprimé&statut=Disponible&nom=Docker&theme=DevOps&prix=10.0`

Cette méthode permet de rechercher un ou plusieurs utilisateurs à partir des champs suivants :

* statut : nécessite un paramètre de type `String`, ex : Disponible,
* nom : nécessite un paramètre de type `String`, ex : Kubernetes,
* theme : nécessite un paramètre de type `String`, ex : DevOps,
* prix : nécessite un paramètre de type `Double`, ex : 0.0.

Si vous souhaitez tester avec plusieurs statuts, il faut le faire de la manière suivante :

* {{baseUrl}}/cours/?statut=Disponible&statut=Supprimé

* URL pour tester : `https://localhost:8084/cours/?statut=Disponible&nom=Docker` ou `{{baseUrl}}/cours/?statut=Disponible&nom=Docker`

la liste des différentes recherches possibles :

* findAllByNomAndThemeAndPrixAndStatutIn(nom, theme, prix, statut);
* findAllByNomAndThemeAndStatutIn(nom, theme, statut);
* findByNomAndThemeAndPrix(nom, theme, prix);
* findByNomAndTheme(nom, theme);
* findAllByNomAndStatutIn(nom, statut);
* findAllByThemeAndStatutIn(theme, statut);
* findAllByThemeAndStatutIn(theme, statut);
* findAllByStatutIn(statut);
* findByNomOrTheme(nom,theme);

Avec un ou plusieurs statut, un nom, un thème et un prix.

Avec HATOES, il y a cinq liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour modifier le cours,
* un pour supprimer le cours.

Exemple, vous pouvez essayer avec un ID fixe d'un cours (le cours est dans le script de peuplement de la base) -> `de5efccc-5372-11eb-ae93-0242ac130002`.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Modifier un cours (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un cours (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des cours": {
        "href": "http://localhost:8084/cours"
    }
}
``` 

On retourne une liste de cours correspondants aux filtres (`status OK`).
Si aucun cours n'est trouvé, il retourne un URI avec la collection des cours (/cours) qui permet d'accéder à la ressource qui liste tous les cours (`status NOT_FOUND`).

### Ajouter un nouvel cours

`Méthode : saveCourse, Type : POST`

* URL : `https://localhost:8084/cours` ou `{{baseUrl}}/cours`

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "description": "Python pour les nuls",
    "nom": "Python",
    "statut": "Disponible",
    "theme": "Programmation",
    "prix": 20.5,
    "note": 6.0
}
```

L'affichage est possible car on retourne dans la méthode saveCourse, une réponse HTTP `201 created` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.status(201).header("location", location.toString()).body(courseToResource(saved, true));
```

Avec HATOES, il y a cinq liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour modifier le cours,
* un pour supprimer le cours.
```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Modifier un cours (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un cours (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des cours": {
        "href": "http://localhost:8084/cours"
    }
}
``` 

S'il a un mauvais format des données (body invalide), on retourne une réponse BadRequest `400 BAD_REQUEST`.Si le cours existe déjà, il y aura une réponse `409 Conflict`.

### Modifier un cours

`Méthode : updateCourse, Type : PUT`

* URL : `https://localhost:8084/cours/:courseId` ou `{{baseUrl}}/cours/:courseId`

Vous pouvez tester avec l'exemple ci-dessous :

`URL de test` : {{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002

```
{
    "description": "Python pour les nuls",
    "nom": "Python",
    "statut": "Disponible",
    "theme": "Programmation",
    "prix": 20.5,
    "note": 6.0
}
```

<span style="color:red">Note importante</span> : l'id est affiché dans le body JSON, mais il n'est pas nécessaire par contre, il est demandé en temps que paramètre dans l'endpoint. 

L'affichage est possible car on retourne dans la méthode updateCourse, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.ok(courseToResource(result, true));
```

Avec HATOES, il y a cinq liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour modifier le cours,
* un pour supprimer le cours.
```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Modifier un cours (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un cours (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des cours": {
        "href": "http://localhost:8084/cours"
    }
}
``` 

Si le body est vide ou que le cours n'exsite pas ou qu'il a été supprimé, il y aura une réponse `404 NOT_FOUND`, avec un message informant que le cours n'existe pas ou plus.

### Modifier un cours de manière partielle

`Méthode : updateCoursePartiel, Type : PATCH`

* URL : `https://localhost:8084/cours/:courseId` ou `{{baseUrl}}/cours/:courseId`

`URL de test` : {{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "description": "Python pour les nuls",
    "nom": "Python",
    "statut": "Disponible",
    "theme": "Programmation",
    "prix": 20.5,
    "note": 6.0
}
```

On récupère le contenu du body (d'où l'utilisation de la Map, qui permet de récupérer un ensemble de clés/valeurs, entre autre les éléments JSON), car il y peut y avoir un ou plusieurs champs.On retourne une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.ok(courseToResource(result, true));
```

Avec HATOES, il y a cinq liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour modifier le cours,
* un pour supprimer le cours.
```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Modifier un cours (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Supprimer un cours (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Liste des cours": {
        "href": "http://localhost:8084/cours"
    }
}
``` 

Si le body est vide ou que le cours n'exsite pas ou qu'il a été supprimé, il y aura une réponse `404 NOT_FOUND`, avec un message informant que le cours n'existe pas ou plus.

### Supprimer un cours

`Méthode : deleteCourse, Type : DELETE`

* URL : `https://localhost:8084/cours/:courseId` ou `{{baseUrl}}/cours/:courseId`

`URL de test` : {{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002

L'affichage est possible car on retourne dans la méthode updateUserPartiel, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.ok(courseToResource(result, true));
```

Avec HATOES, il y a trois liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "episodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Liste des cours": {
        "href": "http://localhost:8084/cours"
    }
}
```

Si le body est vide ou que le cours n'exsite pas ou qu'il a été supprimé, il y aura une réponse `404 NOT_FOUND`, avec un message informant que le cours n'existe pas ou plus.

### Lister tous les épisodes d'un cours

`Méthode : getAllEpisodesCourse, Type : GET`

Cette méthode permet de lister l'ensemble des épisodes d'un cours (`status 200 OK`).

* URL : `https://localhost:8084/cours/:courseId/episodes` ou `{{baseUrl}}/cours/:courseId/episodes`

* `URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes) 

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à un épisode en particulier,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder au cours,
* un pour ajouter un nouvel épisode à un cours.


```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/43179c15-6ef7-4fbd-a9c9-ccc36bcafcc2"
    }
}
...
"_links": {
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Ajouter un nouvel épisode": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Cours": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    }
}
```

### Accéder aux ressources d'un épisode en particulier

`Méthode : getEpisode, Type : GET`

Cette méthode permet de lister l'ensemble des épisodes d'un cours.

* URL : `https://localhost:8084/cours/:courseId/episodes` ou `{{baseUrl}}/cours/:courseId/episodes`

* `URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002) 

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à un cours en particulier,
* un pour accéder à la liste des épisodes du cours,
* un pour modifier l'épisode,
* un pour supprimer l'épisode.

Exemple, vous pouvez essayer avec un ID fixe d'un cours (le cours est dans le script de peuplement de la base) -> `de5efccc-5372-11eb-ae93-0242ac130002` et un pour l'épisode -> `de5efccc-5245-11eb-ae93-0241ac130002`

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Modifier un épisode (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Supprimer un épisode (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    }
}
```

Si le cours est retrouvé, on affiche son contenu (`status OK`) sinon on retourne une réponse 
indiquant que la ressource n'a pas été retrouvée (`status NOT_FOUND`).

### Ajouter un nouvel épisode dans un cours

`Méthode : saveEpisode, Type : POST`

* URL : `https://localhost:8084/cours/{courseId}/episodes` ou `{{baseUrl}}/cours/{courseId}/episode`

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "course": {
        "id": "de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "titre": "Test",
    "url": "test",
    "statut": "Disponible"
}
```

* `URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes) 

L'affichage est possible car on retourne dans la méthode saveCourse, une réponse HTTP `201 created` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.status(201).header("location", location.toString()).body(episodeToResource(courseId, saved, true));
```

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à un cours en particulier,
* un pour accéder à la liste des épisodes du cours,
* un pour modifier l'épisode,
* un pour supprimer l'épisode.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Modifier un épisode (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Supprimer un épisode (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    }
}
```

S'il a un mauvais format des données (body invalide), on retourne une réponse BadRequest `400 BAD_REQUEST`. Si l'épisode existe déjà, il y aura une réponse `409 Conflict`.

### Modifier un épisode

`Méthode : updateEpisode, Type : PUT`

* URL : `https://localhost:8084/cours/:courseId/episodes/:episodeId` ou `{{baseUrl}}/cours/:courseId/episodes/:episodeId`

Vous pouvez tester avec l'exemple ci-dessous :

* `URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002) 

```
{
    "titre": "test",
    "url": "test",
    "statut": "Disponible"
}
```

<span style="color:red">Note importante</span> : l'id est affiché dans le body JSON, mais il n'est pas nécessaire par contre, il est demandé en temps que paramètre dans l'endpoint. 

L'affichage est possible car on retourne dans la méthode updateCourse, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.ok(episodeToResource(courseId, result, true));
```

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à un cours en particulier,
* un pour accéder à la liste des épisodes du cours,
* un pour modifier l'épisode,
* un pour supprimer l'épisode.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Modifier un épisode (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Supprimer un épisode (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    }
}
```

Si le body est vide ou que l'épisode n'exsite pas ou qu'il a été supprimé, il y aura une réponse `404 NOT_FOUND`, avec un message informant que l'épisode n'existe pas ou plus. S'il y a un problème avec le format des données, on retourne un code `400 BAD_REQUEST`.

### Modifier un épisode de manière partielle

`Méthode : updateEpisodePartiel, Type : PATCH`

* URL : `https://localhost:8084/cours/:courseId/episodes/:episodeId` ou `{{baseUrl}}/cours/:courseId/episodes/:episodeId`


`URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002) 

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "titre": "test",
    "url": "test",
    "statut": "Disponible"
}
```

On récupère le contenu du body (d'où l'utilisation de la Map, qui permet de récupérer un ensemble de clés/valeurs, entre autre les éléments JSON), car il y peut y avoir un ou plusieurs champs.On retourne une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.ok(courseToResource(result, true));
```

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à un cours en particulier,
* un pour accéder à la liste des épisodes du cours,
* un pour modifier l'épisode,
* un pour supprimer l'épisode.

```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Modifier un épisode (PUT/PATCH)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Supprimer un épisode (DELETE)": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    }
}
```

Si le body est vide ou que l'épisode n'exsite pas ou qu'il a été supprimé, il y aura une réponse `404 NOT_FOUND`, avec un message informant que l'épisode n'existe pas ou plus.

### Supprimer un épisode d'un cours

`Méthode : deleteEpisode, Type : DELETE`

* URL : `https://localhost:8084/cours/:courseId/episodes/:episodeId` ou `{{baseUrl}}/cours/:courseId/episodes/:episodeId`

`URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002) 

L'affichage est possible car on retourne dans la méthode deleteEpisode, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.ok(episodeToResource(courseId, episode, true));
```

Avec HATOES, il y a deux liens qui sont générés :

* un pour accéder à un épisodes en particulier,
* un pour accéder à la liste des épisodes du cours.


```
"_links": {
    "self": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8084/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    }
}
```

Si le body est vide ou que l'épisode n'exsite pas ou qu'il a été supprimé, il y aura une réponse `404 NOT_FOUND`, avec un message informant que l'épisode n'existe pas ou plus.