# Projet M2 SID API

réalisé par Anne-Laure CHARLES.

Date de rendu  : 29/01/2021.

## Contenu

Sur ce dépôt vous trouverez les services utilisés pour l'API.

* adminsitration-service : service dédié aux administrateur;
* user-service : service dédié aux utilisateurs du site de cours;

Le rapport du projet se trouve également sur le dépôt et chaque service possède son script de peupelement de base de données.

## Récupérer le projet

```
git clone 
``` 

## Manipuler les services

* administration-service port : 8084
* user-service port :8085

### Pour lancer le service

Générer l'executable avec MAVEN.

```
mvn -DskipTests clean install
```

Démarrer le service

```
java -jar target/administration-service-v1.jar
```

ou 

```
java -jar target/user-service-v1.jar
```

### Tester

#### Importer sur Postman

* Pour importer les routes du service administration, aller sur l'URL [Open API Admin](http://localhost:8084/v3/api-docs/)
* Pour importer les routes du service utilisateur, aller sur l'URL [Open API User](http://localhost:8085/v3/api-docs/)

#### Utiliser Swagger

Vous pouvez tester vos requêtes en vous rendant sur les URLs suivantes :

* [Open API Admin](http://localhost:8084/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#)
* [Open API User](http://localhost:8085/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#)

#### Jeu de données avec URL + BODY

Les jeux de données sont disponibles dans les ReadMe.md des services respectifs.

### Évolution de la base de données

Comme expliqué les deux bases de données des deux services, ne sont pas synchronisés en une seule. Pour des raisons expliquées dans le rapport du projet, voici le script 
de configuration pour avoir une seule et même base au sein de l'API :

```
spring.application.name=online-courses
server.port=8082
server.error.include-stacktrace: never

spring.jpa.show-sql=true

# permet d'avoir le répertoire au même niveau que les rep /user-service & /administration-service
spring.datasource.url=jdbc:h2:file:../db;DB_CLOSE_ON_EXIT=FALSE;AUTO_SERVER=TRUE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.platform=h2
spring.jpa.hibernate.ddl-auto=update
spring.datasource.initialization-mode=always

spring.h2.console.enabled=true
spring.h2.console.path=/console
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
```

## Licence

MIT Licence
