package org.miage.userservice;

import org.junit.jupiter.api.Test;
import org.miage.userservice.entity.users.User;
import org.miage.userservice.entity.courses.Course;
import org.miage.userservice.entity.episodes.Episode;
import org.miage.userservice.boundary.courses.CourseResource;
import org.miage.userservice.boundary.users.EpisodeResource;
import org.miage.userservice.boundary.users.UserResource;
import org.miage.userservice.core.UserStatusEnum;
import org.springframework.boot.test.context.SpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.UUID;
import org.apache.http.HttpStatus;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class UserServiceApplicationTests {

	@LocalServerPort
    int port;

    @Autowired
    UserResource ur;
    
    @Autowired
    CourseResource cr;

    @BeforeEach
    public void setupContext() {
        ur.deleteAll();
        RestAssured.port = port;
    }
    
    @Test
    public void getOneUser() {
    	User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER",  UserStatusEnum.NOUVEAU , "France");
        ur.save(u1);
        Response response = when().get("/utilisateurs/" + u1.getId())
                .then().statusCode(HttpStatus.SC_OK).extract().response();
        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString("Tom"));
    }

    @Test
    public void postUser() throws Exception {
    	User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER",  UserStatusEnum.NOUVEAU , "France");
        Response response = given().body(this.toJsonString(u1)).contentType(ContentType.JSON).when()
                .post("/utilisateurs").then().statusCode(HttpStatus.SC_CREATED).extract().response();
        String location = response.getHeader("Location");
        when().get(location).then().statusCode(HttpStatus.SC_OK);
    }
    
    ///////////////////////////////////////////////////// COURSES SUB //////////////////////////////////////////////////////////////////////////////
    
    @Test
    public void pingCourses() {
    	User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER",  UserStatusEnum.NOUVEAU , "France");
        ur.save(u1);
        when().get("/utilisateurs/" + u1.getId() + "/cours").then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void getAllCourses() {
	   User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER",  UserStatusEnum.NOUVEAU , "France");
       ur.save(u1);
       Course c1 = new Course(UUID.randomUUID().toString(), "Programmation Java", "Programmation", "cours débutants/avancés", "Disponible", 0.0, 9.0);
       cr.save(c1);
       Course c2 = new Course(UUID.randomUUID().toString(), "Programmation Python", "Programmation", "cours débutants/avancés", "Disponible", 10.0, 4.0);
       cr.save(c2);
       u1.getCours().add(c1);
       u1.getCours().add(c2);
       when().get("/utilisateurs/" + u1.getId() + "/cours").then().statusCode(HttpStatus.SC_OK);
    }
    
    ///////////////////////////////////////////////////// COURSES SUB //////////////////////////////////////////////////////////////////////////////
    
	@Test
	public void pingEpisodes() {
		User u1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "tom-sawyer@gmail.com", "USER",  UserStatusEnum.NOUVEAU , "France");
		ur.save(u1);
		when().get("/utilisateurs/" + u1.getId() + "/episodes-vus").then().statusCode(HttpStatus.SC_OK);
	}
    
   private String toJsonString(Object r) throws Exception {
       ObjectMapper map = new ObjectMapper();
       return map.writeValueAsString(r);
   }


}
