package org.miage.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class UserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}
	
	@Bean
	//http://localhost:8085/v3/api-docs/
	public OpenAPI customOpenApi() {
		
		return new OpenAPI().info(new Info().title("User API").version("1.0")
				.description("Documentation Utilisateur API MIAGE 2 - SID"));
	}

}
