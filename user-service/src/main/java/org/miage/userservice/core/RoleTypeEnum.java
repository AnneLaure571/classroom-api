package org.miage.userservice.core;

public enum RoleTypeEnum {
	
	ADMIN,
	PROFESSOR,
	STUDENT,
	USER;

}
