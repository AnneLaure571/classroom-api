package org.miage.userservice.core;

public enum UserStatusEnum {
	
	NOUVEAU,
	BON_ELEVE,
	MAUVAIS_PAYEUR,
	SUPPRIMÉ

}
