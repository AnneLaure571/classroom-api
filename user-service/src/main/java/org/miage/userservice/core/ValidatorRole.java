package org.miage.userservice.core;



import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidatorRole implements ConstraintValidator<ValidateRole, String> { 
	
	private ValidateRole annotation;
	 
    @Override
    public void initialize(ValidateRole annotation)
    {
        this.annotation = annotation;
    }
 
    @Override
    public boolean isValid(String valueForValidation, ConstraintValidatorContext constraintValidatorContext)
    {
        boolean result = false;
         
        Object[] enumValues = this.annotation.enumClass().getEnumConstants();
         
        if(enumValues != null)
        {
            for(Object enumValue:enumValues)
            {
                if(valueForValidation.equals(enumValue.toString()))
                {
                    result = true; 
                    break;
                }
            }
        }
         
        return result;
    }
} 