package org.miage.userservice.boundary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = " Impossible de visionner l'épisode, vous n'avez pas acheté le cours !")
public class EpisodeViewedBadRequestException extends RuntimeException {

}