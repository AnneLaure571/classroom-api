package org.miage.userservice.boundary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Erreur : il existe déjà un nom avec ce cours")
public class CourseExistsException extends RuntimeException {

}
