package org.miage.userservice.boundary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = " Un utilisateur existe déjà avec cet email ! ")
public class UserExistsException extends RuntimeException {

}