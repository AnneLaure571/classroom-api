package org.miage.userservice.boundary.users;

import java.util.List;
import java.util.Optional;

import org.miage.userservice.entity.courses.Course;
import org.miage.userservice.entity.episodes.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(collectionResourceRel="course")

public interface EpisodeResource extends JpaRepository<Episode, String>{

	Iterable<Episode> findByCourseId(String courseId);
	
	Optional<Episode> findByCourseIdAndId(String courseId, String id);
	
	public Course getByCourse(String courseId);

	Iterable<Episode> findAllByUtilisateurs_Id(String userId);

}
