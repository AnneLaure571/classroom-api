package org.miage.userservice.boundary.users;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.miage.userservice.entity.courses.CourseValidator;
import org.miage.userservice.entity.episodes.EpisodeValidator;
import org.miage.userservice.entity.purchases.Purchase;
import org.miage.userservice.boundary.courses.CourseRepresentation;
import org.miage.userservice.boundary.courses.CourseResource;
import org.miage.userservice.boundary.exceptions.EpisodeViewedBadRequestException;
import org.miage.userservice.core.UserStatusEnum;
import org.miage.userservice.entity.courses.Course;
import org.miage.userservice.entity.courses.CourseInput;
import org.miage.userservice.entity.episodes.Episode;
import org.miage.userservice.entity.users.*;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.util.ReflectionUtils;

@RestController
@RequestMapping(value="/utilisateurs", produces= MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(User.class)
@RequiredArgsConstructor
public class UserRepresentation {
	
	private final UserResource ur;
	// add validator
	private final UserValidator uv;
	
	private final CourseResource cr;
	// add validator
	private final CourseValidator cv;
	
	private final EpisodeResource epr;
	// add validator
	private final EpisodeValidator epv;
	
	// Login
	@GetMapping(value="/login")
	public ResponseEntity<?> login(@RequestParam(value="email") String email){
		Optional<User> existingEmail = ur.findByEmail(email);
		//check if email already exists 
		if (!existingEmail.isPresent()) {
			return ResponseEntity.status(404).header("header", "invalid-user").body("Aucun utilisateur correspondant !");
		}
		User user = existingEmail.get();
		return ResponseEntity.ok(userToResource(user));
	}
	
	// GET One : user profile 
	@GetMapping(value="/{userId}")
	public ResponseEntity<?> getProfile(@PathVariable("userId") String id){
		return Optional.ofNullable(ur.findById(id)).filter(Optional::isPresent)
				.map(i -> ResponseEntity.ok(userToResource(i.get())))
				.orElse(ResponseEntity.notFound().build());
	}
	
	// POST : subscribe 
	@PostMapping
	@Transactional
	public ResponseEntity<?> subscribeUser(@RequestBody @Valid UserInput user){
		Optional<User> existingUser = ur.findByEmail(user.getEmail());
		//check if email already exists 
		if (existingUser.isPresent()) {
			return ResponseEntity.status(409).header("header", "conflict").body("Un utilisateur existe déjà avec cet email !");
		}
		User u = new User(UUID.randomUUID().toString(), user.getNom(), user.getPrenom(), user.getEmail(),
				"USER", UserStatusEnum.NOUVEAU, user.getPays(), new ArrayList<>(), new ArrayList<>());
		User saved = ur.save(u);
		// slash allow to add sub resource in URI
		URI location = linkTo(UserRepresentation.class).slash(saved.getId()).toUri();
		return ResponseEntity.status(201).header("location", location.toString()).body(userToResource(saved));
	}

	// PATCH : updateProfile
    @PatchMapping(value = "/{userId}")
    @Transactional
    public ResponseEntity<?> updateProfile(@PathVariable("userId") String userId,
            @RequestBody @Valid Map<Object, Object> fields) {
        Optional<User> body = ur.findById(userId);
        if (body.isPresent()) {
            User user = body.get();
            fields.forEach((f, v) -> {
                Field field = ReflectionUtils.findField(User.class, f.toString());
                field.setAccessible(true);
                ReflectionUtils.setField(field, user, v);
            });
            if (!ur.existsById(userId) || user.getStatut().equals("SUPPRIMÉ")) {
            	return ResponseEntity
          		      .status(404)
          		      .header("header", "user-invalid")
          		      .body("L'utilisateur est introuvable ou supprimé !");
    		}
            uv.validate(new UserInput(user.getNom(), user.getPrenom(),user.getEmail(),
            		 user.getPays()));
            user.setId(userId);
            ur.save(user);
            return ResponseEntity.ok(userToResource(user));
        }
        return ResponseEntity.notFound().build();
    }
    
    ///////////////////////////////////////// COURSES PART /////////////////////////////////////////////////////////
 	
    // GET All cours subscribed
    @GetMapping(value="/{userId}/cours")
    @Transactional
    public ResponseEntity<?> getAllCoursesSubUser(@PathVariable("userId") String userId) {
 		Iterable<Course> all = cr.findAllByUtilisateurs_Id(userId);
 		//With HATOES, return list  of courses of one User
 		return ResponseEntity.ok(userToResource(all, userId));
    }
    
    // GET One course
    @GetMapping(value="/{userId}/cours/{courseId}")
    @Transactional
    public ResponseEntity<?> getCourseSubscribed(@PathVariable("userId") String userId, @PathVariable("courseId") String courseId) {
    	//get the user
    	Optional<User> userFound = ur.findById(userId);
		if (!userFound.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		User user = userFound.get();
		Course found = null;
		if (!(user.getStatut().equals("SUPPRIMÉ"))){
			Course cours = cr.findById(courseId).get();
			for(Course courseToFind : user.getCours()){
				//If in id courses List == cours find by id
				if (courseToFind.getId() == cours.getId()) {
			    	found = courseToFind;
				}
			}
		} else {
			return ResponseEntity.status(404).header("header", "user-invalid").body("L'utilisateur est introuvable ou supprimé !");
		}
		if (found == null) {
			return ResponseEntity.badRequest().body("Il n'y a aucun cours correspondant dans la liste de l'utilisateur !");
		}
		return ResponseEntity.ok(userToResource(userId, found, true));
    	
    }
    
    //Unsubscribe
    @DeleteMapping("/{userId}/unsubscribe")
    @Transactional
	public ResponseEntity<?> unsubscribeCourse(@PathVariable("userId") String userId, @RequestParam(value="courseId")  String courseId) {
    	//Get the user
    	Optional<User> optionalUser = ur.findById(userId);
		if (!optionalUser.isPresent()) {
			return ResponseEntity.status(404).header("header", "user-invalid").body("L'utilisateur est introuvable ou supprimé !");
		}
		
		User user = optionalUser.get();

		if (!user.containsCourse(courseId)) {
			return ResponseEntity.badRequest().body("Impossible de se désincrire.");
		}
		Course course = cr.findById(courseId).get();
		user.getCours().remove(course);
		User result = ur.save(user);

		return ResponseEntity.ok(coursesAddedListLink(result.getId()));
	}
    
    ///////////////////////////////////////// EPISODES PART /////////////////////////////////////////////////////////
    
    @GetMapping(value="/{userId}/episodes-vus")
    @Transactional
    public ResponseEntity<?> getAllViewedEpisodes(@PathVariable("userId") String userId) {
    	Iterable<Episode> all = epr.findAllByUtilisateurs_Id(userId);
 		//With HATOES, return list  of episodesViewed of one User
 		return ResponseEntity.ok(episodeToResource(all, userId));
    }
    
    @GetMapping(value="/{userId}/episodes-vus/{episodeId}")
    @Transactional
    public ResponseEntity<?> getEpisodeViewed(@PathVariable("userId") String userId,  @PathVariable("episodeId") String episodeId) {
    	//get the user
    	Optional<User> userFound = ur.findById(userId);
		if (!userFound.isPresent()) {
			return ResponseEntity.status(404).header("header", "user-invalid").body("L'utilisateur est introuvable ou supprimé !");
		}
		User user = userFound.get();
		Episode found = null;
		if (!(user.getStatut().equals("SUPPRIMÉ"))){
			Episode episode = epr.findById(episodeId).get();
			for(Episode episodeToFind : user.getEpisodesVus()){
				//If in id courses List == cours find by id
				if (episodeToFind.getId() == episode.getId()) {
			    	found = episodeToFind;
				}
			}
		} 
		if (found == null) {
			return ResponseEntity.badRequest().body("Il n'y a aucun episode correspondant dans la liste de l'utilisateur !");
		}
		return ResponseEntity.ok(episodeToResource(userId, found, true));
    }
    
    // Access episodes from the CourseSub
    @GetMapping(value="/{userId}/cours/{courseId}/episodes")
    @Transactional
    public ResponseEntity<?> getCourseEpisodesSubscribed(@PathVariable("userId") String userId, @PathVariable("courseId") String courseId) {
    	//get the user
    	Optional<User> userFound = ur.findById(userId);
		if (!userFound.isPresent()) {
			return ResponseEntity.status(404).header("header", "user-invalid").body("L'utilisateur est introuvable ou supprimé !");
		}
		User user = userFound.get();
		Course found = null;
		if (!(user.getStatut().equals("SUPPRIMÉ"))){
			Course cours = cr.findById(courseId).get();
			for(Course courseToFind : user.getCours()){
				//If in id courses List == cours find by id
				if (courseToFind.getId() == cours.getId()) {
			    	found = courseToFind;
				}
			}
		} 
		if (found == null) {
			return ResponseEntity.badRequest().body("Il n'y a aucun cours correspondant dans la liste de l'utilisateur !");
		}
		Iterable<Episode> all = epr.findByCourseId(found.getId());
		return ResponseEntity.ok(episodeToResource(all, courseId, userId));
    }

	/*
     * {{baseUrl}}/utilisateurs/c0b7a868-5372-11eb-ae93-0242ac130002/episodes-vus/de5efccc-5245-11eb-ae93-0241ac130002
     */
    /*@PutMapping(value="/{userId}/episodes-vus/{episodeId}")
    @Transactional
    public ResponseEntity<?> addViewedEpisode(@PathVariable("userId") String userId, @PathVariable("episodeId") String episodeId) {
    	//Get the episode
		Episode episodeViewed = epr.findById(episodeId).get();
    	//Get the user
    	Optional<User> optionalUser = ur.findById(userId);
		if (!optionalUser.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		User user = optionalUser.get();
		
		// Add episodeViewed in user list
		user.getEpisodesVus().add(episodeViewed);

		ur.save(user);

		return ResponseEntity.noContent().build();

    }*/
    
    // View Episode
    @PutMapping(value="/{userId}/cours/{courseId}/episodes/{episodeId}/view")
    @Transactional
    public ResponseEntity<?> addViewedEpisode(@PathVariable("userId") String userId, @PathVariable("courseId") String courseId,
    		@PathVariable("episodeId") String episodeId) throws EpisodeViewedBadRequestException {
    	//get the user
    	Optional<User> userFound = ur.findById(userId);
		if (!userFound.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		if (!ur.existsById(userId)) {
			return ResponseEntity
        		      .status(404)
        		      .header("header", "invalid-request")
        		      .body("Vous n'avez pas acheté le cours, impossible de visioner !");
		}
		User user = userFound.get();
		Course found = null;
		Episode episodeViewed =null;
		if (!(user.getStatut().equals("SUPPRIMÉ"))){
			Course cours = cr.findById(courseId).get();
			for(Course courseToFind : user.getCours()){
				//If in id courses List == cours find by id
				if (courseToFind.getId() == cours.getId()) {
			    	found = courseToFind;    	
				}
			}
			//check if the episode is in Course List
			Episode episodeFound = epr.findById(episodeId).get();
			for(Episode episodeToFind : found.getEpisodes()){
				//If in id courses List == cours find by id
				if (episodeToFind.getId() == episodeFound.getId()) {
					episodeViewed = episodeToFind;    	
				}
			
			}
		} 
		if (found == null) {
			return ResponseEntity.badRequest().body("Il n'y a aucun cours correspondant dans la liste de l'utilisateur !");
		}
		// Add episodeViewed in user list
		user.getEpisodesVus().add(episodeViewed);
		ur.save(user);
		return ResponseEntity.ok(episodeToResource(userId, courseId, episodeViewed, true));
    }
    
    ///////////////////////////////////////// HATOES PART ///////////////////////////////////////////////////////////////
    
    //Static methods
    
    private static Link selfLink(User user) {
  		return linkTo(methodOn(UserRepresentation.class).getProfile(user.getId())).withSelfRel();
  	}
    
    private static Link userLink(String userId) {
  		return linkTo(methodOn(UserRepresentation.class).getProfile(userId)).withRel("Utilisateur");
  	}
    
    private static Link coursesAddedLink(User user) {
    	return linkTo(UserRepresentation.class)
        		.slash(user.getId())
        		.slash("cours")
        		.withRel("Cours-suivis");
    }
    
    private static Link coursesAddedListLink(String userID) {
    	return linkTo(methodOn(UserRepresentation.class)
 				.getAllCoursesSubUser(userID))
    			.withRel("Cours-suivis");
    }
    
    private static Link coursesListLink() {
    	return linkTo(methodOn(CourseRepresentation.class).getAllCourses()).withRel("Catalogue cours");
    }
    
    private static Link courseSubLink(String userID, Course course) {
    	return linkTo(UserRepresentation.class)
        		.slash(userID)
        		.slash("cours")
        		.slash(course.getId())
        		.withSelfRel();
    }
    
    private static Link subscribeUserLink() {
  		return linkTo(methodOn(UserRepresentation.class).subscribeUser(null)).withRel("S'inscrire sur le site");
  	}
    
    private static Link updateProfileUserLink(String userID) {
  		return linkTo(methodOn(UserRepresentation.class).updateProfile(userID, null)).withRel("Modifier son profil (PATCH)");
  	}
    
    
    private static Link courseEpisodesSubLink(String userID, Course course) {
    	//return linkTo(methodOn(CourseRepresentation.class).getAllEpisodesCourse(course.getId())).withRel("Episodes");
    	return linkTo(methodOn(UserRepresentation.class).getCourseEpisodesSubscribed(userID, course.getId())).withRel("Episodes");
    }
    
    private static Link unSubscribeCourseLink(String userID, Course course) {
  		return linkTo(methodOn(UserRepresentation.class).unsubscribeCourse(userID, course.getId())).withRel("Se désinscrire du cours");
  	}
    
    private static Link episodesViewedListLink(String userID) {
  		return linkTo(methodOn(UserRepresentation.class).getAllViewedEpisodes(userID)).withRel("Épisodes vus");
  	}
    
    private static Link viewEpisodeLink(String userID, String courseID, String episodeID) {
  		return linkTo(methodOn(UserRepresentation.class).addViewedEpisode(userID, courseID, episodeID)).withRel("Visionner l'épisode");
  	} 
    
    //////////////////////////////////////////  HATOES USER ///////////////////////////////////////////////
    
    //Login +  GetOne + subscribeUser
    private EntityModel<User> userToResource(User user) {
        return EntityModel.of(user, selfLink(user), updateProfileUserLink(user.getId()), coursesAddedLink(user), 
        		episodesViewedListLink(user.getId()), coursesListLink());
    }
    
    // GetAllCoursesUser
    private CollectionModel<EntityModel<Course>>userToResource(Iterable<Course> courses, String userID) {
         List<EntityModel<Course>> coursResources = new ArrayList<EntityModel<Course>>();
         courses.forEach(course -> coursResources.add(userToResource(userID, course, false)));
         return CollectionModel.of(coursResources, coursesAddedListLink(userID), userLink(userID));
     }
    
    // GetOneCourseSub
    public EntityModel<Course> userToResource(String userID, Course course, boolean collection) {
        if (Boolean.TRUE.equals(collection)) {
            return EntityModel.of(course, courseSubLink(userID,course), 
            		courseEpisodesSubLink(userID, course),  
            		unSubscribeCourseLink(userID, course),
            		coursesAddedListLink(userID));
        } else {
            return EntityModel.of(course, courseSubLink(userID,course));
        }
	}
    
    // UnSubscribeCourse
    private EntityModel<Course> courseToResource(Course course) {
        var selfLink = linkTo(UserRepresentation.class).slash(course.getId()).withSelfRel();
        return EntityModel.of(course, selfLink);
    }
    
    
    //////////////////////////////////////////////// HATOES EPISODES/USER /////////////////////////
    
    private CollectionModel<EntityModel<Episode>>episodeToResource(Iterable<Episode> episodes, String userID) {
 		
 		Link selfLink = linkTo(methodOn(UserRepresentation.class)
 				.getAllViewedEpisodes(userID))
 				.withSelfRel();
         List<EntityModel<Episode>> episodeResource = new ArrayList<EntityModel<Episode>>();
         episodes.forEach(episode -> episodeResource.add(episodeToResource(userID, episode, false)));
         return CollectionModel.of(episodeResource, selfLink, userLink(userID));
     }
    
    private EntityModel<Episode> episodeToResource(String userID, Episode episode, boolean collection) {
    	var selfLink = linkTo(UserRepresentation.class)
        		.slash(userID)
        		.slash("episodes-vus")
        		.slash(episode.getId())
        		.withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            return EntityModel.of(episode, selfLink, episodesViewedListLink(userID));
        } else {
            return EntityModel.of(episode, selfLink );
        }
	}
    
    private CollectionModel<EntityModel<Episode>>episodeToResource(Iterable<Episode> episodes, String courseId, String userID) {	
 		Link selfLink = linkTo(methodOn(UserRepresentation.class).getAllCoursesSubUser(userID)).slash(courseId).withRel("Liste des épisodes");
 		Link courseLink = linkTo(methodOn(UserRepresentation.class).getAllCoursesSubUser(userID)).withRel("Cours");
         List<EntityModel<Episode>> episodeResource = new ArrayList<EntityModel<Episode>>();
         episodes.forEach(episode -> episodeResource.add(episodeToResource(userID, courseId, episode, false)));
         return CollectionModel.of(episodeResource, selfLink, courseLink );
     }
    
    private EntityModel<Episode> episodeToResource(String userID, String courseId, Episode episode, boolean collection) {
    	var selfLink = linkTo(UserRepresentation.class)
        		.slash(userID)
        		.slash("episodes-vus")
        		.slash(episode.getId())
        		.withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            return EntityModel.of(episode, selfLink, episodesViewedListLink(userID));
        } else {
            return EntityModel.of(episode, viewEpisodeLink(userID, courseId, episode.getId()));
        }
	}
    

}