package org.miage.userservice.boundary.courses;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.miage.userservice.boundary.exceptions.BadRequestException;
import org.miage.userservice.boundary.exceptions.EpisodeViewedBadRequestException;
import org.miage.userservice.boundary.users.EpisodeResource;
import org.miage.userservice.boundary.users.UserRepresentation;
import org.miage.userservice.boundary.users.UserResource;
import org.miage.userservice.entity.courses.*;
import org.miage.userservice.entity.episodes.Episode;
import org.miage.userservice.entity.episodes.EpisodeInput;
import org.miage.userservice.entity.episodes.EpisodeValidator;
import org.miage.userservice.entity.purchases.Purchase;
import org.miage.userservice.entity.users.User;
import org.miage.userservice.entity.users.UserValidator;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.util.ReflectionUtils;

@RestController
// not need to define @RequestBody, restcontroller specific versionof controller
// RequestMapping, allow to define URI (value), and the result produces
@RequestMapping(value="/cours", produces= MediaType.APPLICATION_JSON_VALUE)
// HATEOAS create links based on model user
@ExposesResourceFor(Course.class)
@RequiredArgsConstructor
public class CourseRepresentation {
	
	private final UserResource ur;
	// add validator
	private final UserValidator uv;
	
	private final CourseResource cr;
	// add validator
	private final CourseValidator cv;
	
	private final EpisodeResource epr;
	// add validator
	private final EpisodeValidator epv;
	
	
	//ResponseEntity : allow to set header, status, body... use to configure response
	
	/////////////////////////////// COURSE PART ////////////////////////////////////
	
	// GET all 
	@GetMapping
	public ResponseEntity<?> getAllCourses(){
		// like for each, iterate all element of the Iterable
		Iterable<Course> all = cr.findAll();
		// on retourne tout le http avec le header.. et dans le body on met toute la liste
		//return ResponseEntity.ok(all);
		return ResponseEntity.ok(courseToResource(all));
	}
	
	// GET one 
	@GetMapping(value="/{courseId}")
	public ResponseEntity<?> getCourse(@PathVariable("courseId") String id){
		return Optional.ofNullable(cr.findById(id)).filter(Optional::isPresent)
				//.map(i -> ResponseEntity.ok(i))
				.map(i -> ResponseEntity.ok(courseToResource(i.get(), true)))
				.orElse(ResponseEntity.notFound().build());
	}
	
	// GET by filters 
    @GetMapping(value="/search")
 	public ResponseEntity<?> searchCourse(@RequestParam(value="statut", required=false) List<String> statut,
 			@RequestParam(value="nom", required=false) String nom, 
 			@RequestParam(value="theme", required=false) String theme, @RequestParam(value="prix", required=false) Double prix){
 		
 		Iterable<Course> all;
 		
 		if(nom != null && theme != null && statut != null && prix != null) {
 			all = cr.findAllByNomAndThemeAndPrixAndStatutIn(nom, theme, prix, statut);
 		} else if(nom != null && theme != null && statut != null) {
 			all = cr.findAllByNomAndThemeAndStatutIn(nom, theme, statut);
 		} else if(nom != null && theme != null && prix != null) {
 			all = cr.findByNomAndThemeAndPrix(nom, theme, prix);
 		} else if(nom != null && theme != null) {
 			all = cr.findByNomAndTheme(nom, theme);
 		} else if(statut != null && nom != null) {
 			all = cr.findAllByNomAndStatutIn(nom, statut);
 		} else if(statut != null && theme != null) {
 			all = cr.findAllByThemeAndStatutIn(theme, statut);
 		} else if(statut != null) {
 			all = cr.findAllByStatutIn(statut);
 		} else {
 			all = cr.findByNomOrTheme(nom,theme);
 		}
 		
 		return ResponseEntity.ok(courseToResource(all));
 	}
    
    @PostMapping(value="/{courseId}/subscribe")
    @Transactional
    public ResponseEntity<?> subscribeCourse(@RequestParam(value="userId") String userId, @PathVariable("courseId") String courseId,
    		@RequestBody @Valid Purchase purchase) throws BadRequestException {
    	//Get the course
		Course course = cr.findById(courseId).get();
    	//Get the user
    	Optional<User> optionalUser = ur.findById(userId);
		if (!optionalUser.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		User user = optionalUser.get();
		
		// verification si le cours est déjà acheté
		if (course.getStatut().equals("Supprimé")) {
			return ResponseEntity.status(404).header("header", "course-invalid").body("Le cours est introuvable ou supprimé, Impossible de s'inscrire !");
		}
		
		// verification si le cours est déjà acheté
		if (user.containsCourse(courseId)) {
			 return ResponseEntity.status(409).header("header", "conflict").body("Vous êtes déjà inscrit à ce cours !");
		}
		if (course.feeBased()) {
			if (! (Long.parseLong(purchase.getNumeroCarte()) % 2 == 0)) {
	    		return ResponseEntity.badRequest().body("Numéro de carte Invalide");
	    	}
			if (purchase.getNumeroCarte() == null|| purchase.getCode() == null || purchase.getCodeValidation() == null) {
	    		return ResponseEntity.badRequest().body("Veuillez renseigner les informations au format suivant : { numeroCarte: string, code: string, codeValidation: string }");
	    	}
		}
		
		// Add cours in user list
		user.getCours().add(course);
		System.out.println(user.getCours());
		User saved = ur.save(user);

		URI location = linkTo(UserRepresentation.class).slash(saved.getId()).slash("cours").toUri();
		return ResponseEntity.status(201).header("location", location.toString()).body("Le cours a été ajouté à votre liste de cours !");

    }
    
    ////////////////////////////////// EPISODE PART /////////////////////////////////////////
    
    @GetMapping(value="/{courseId}/episodes")
 	public ResponseEntity<?> getAllEpisodesCourse(@PathVariable("courseId") String courseId){
 		// like for each, iterate all element of the Iterable
 		Iterable<Episode> all = epr.findByCourseId(courseId);
 		// on retourne tout le http avec le header.. et dans le body on met toute la liste
 		return ResponseEntity.ok(episodeToResource(all, courseId));
 	}
    
	// GET one 
  	@GetMapping(value="/{courseId}/episodes/{episodeId}")
  	public ResponseEntity<?> getEpisode(@PathVariable("courseId") String courseId, @PathVariable("episodeId") String id){
  		return Optional.ofNullable(epr.findByCourseIdAndId(courseId, id)).filter(Optional::isPresent)
  				.map(i -> ResponseEntity.ok(episodeToResource(courseId, i.get(), true)))
  				.orElse(ResponseEntity.notFound().build());
  	}
  	

    /*@PutMapping(value="/{courseId}/episodes/{episodeId}/view")
    @Transactional
    public ResponseEntity<?> addViewedEpisode(@RequestParam(value="userId") String userId, @PathVariable("courseId") String courseId,
    		@PathVariable("episodeId") String episodeId) throws EpisodeViewedBadRequestException {
    	//get the user
    	Optional<User> userFound = ur.findById(userId);
		if (!userFound.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		if (!ur.existsById(userId)) {
			return ResponseEntity
        		      .status(404)
        		      .header("header", "invalid-request")
        		      .body("Vous n'avez pas acheté le cours, impossible de visioner !");
		}
		User user = userFound.get();
		Course found = null;
		Episode episodeViewed =null;
		if (!(user.getStatut().equals("SUPPRIMÉ"))){
			Course cours = cr.findById(courseId).get();
			for(Course courseToFind : user.getCours()){
				//If in id courses List == cours find by id
				if (courseToFind.getId() == cours.getId()) {
			    	found = courseToFind;    	
				}
			}
			//check if the episode is in Course List
			Episode episodeFound = epr.findById(episodeId).get();
			for(Episode episodeToFind : found.getEpisodes()){
				//If in id courses List == cours find by id
				if (episodeToFind.getId() == episodeFound.getId()) {
					episodeViewed = episodeToFind;    	
				}
			
			}
		} else {
			return ResponseEntity.badRequest().body("L'utilisateur a été supprimé !");
		}
		if (found == null) {
			return ResponseEntity.badRequest().body("Il n'y a aucun cours correspondant dans la liste de l'utilisateur !");
		}
		
		// Add episodeViewed in user list
		user.getEpisodesVus().add(episodeViewed);
		ur.save(user);

		return ResponseEntity.noContent().build();

    }*/
    
    //#################### RESSOURCE PART ####################
    
    //Static methods 
  	
  	private static Link selfLink(Course course) {
  		return linkTo(CourseRepresentation.class).slash(course.getId()).withSelfRel();
  	}
  	
  	private static Link coursesListLink() {
  		return linkTo(methodOn(CourseRepresentation.class).getAllCourses()).withRel("Liste des cours");
  	}
  	
  	public static Link subscribeCourseLink(Course course) {
  		return linkTo(methodOn(CourseRepresentation.class).subscribeCourse(null, course.getId(), null)).withRel("S'inscrire au cours");
  	}
  	
  	private static Link episodesCourseListLink(Course course) {
  		return linkTo(methodOn(CourseRepresentation.class).getAllEpisodesCourse(course.getId())).withRel("Liste des épisodes");
  	}
  	
  	private static Link searchCourseLink() {
  		return linkTo(methodOn(CourseRepresentation.class).searchCourse(null, null, null, null)).withRel("Rechercher un cours");
  	}
	
  	private CollectionModel<EntityModel<Course>>courseToResource(Iterable<Course> courses) {
        List<EntityModel<Course>> courseResources = new ArrayList<EntityModel<Course>>();
        courses.forEach(course -> courseResources.add(courseToResource(course, false)));
        return  CollectionModel.of(courseResources, coursesListLink(), searchCourseLink());
    }

    private EntityModel<Course> courseToResource(Course course, Boolean collection) { 
        if (Boolean.TRUE.equals(collection)) {
            return EntityModel.of(course, selfLink(course), episodesCourseListLink(course), subscribeCourseLink(course), coursesListLink());
        } else {
            return EntityModel.of(course,selfLink(course),  episodesCourseListLink(course));
        }
    }
    
    
    ////////////////////////////////////  HATOES EPISODE PART //////////////////////////////////////////////////////////////////
    
    //Static methods 
    
    private static Link selfLink(String courseId, Episode episode) {
  		return linkTo(CourseRepresentation.class)
         		.slash(courseId)
         		.slash("episodes")
         		.slash(episode.getId())
         		.withSelfRel();
  	}
    
    private static Link courseEpisodesListLink(String courseId) {
  		return linkTo(methodOn(CourseRepresentation.class).getAllEpisodesCourse(courseId)).withRel("Liste des épisodes");
  	}
  	
  	private static Link courseLink(String courseId) {
  		return linkTo(CourseRepresentation.class).slash(courseId).withRel("Cours");
  	}
    
    private CollectionModel<EntityModel<Episode>>episodeToResource(Iterable<Episode> episodes, String courseId) {
        List<EntityModel<Episode>> episodesResources = new ArrayList<EntityModel<Episode>>();
        episodes.forEach(episode -> episodesResources.add(episodeToResource(courseId, episode, false)));
        return CollectionModel.of(episodesResources, courseEpisodesListLink(courseId), courseLink(courseId));
     }

     private EntityModel<Episode> episodeToResource(String courseId, Episode episode, Boolean collection) {
         if (Boolean.TRUE.equals(collection)) {
             return EntityModel.of(episode, selfLink(courseId,episode), 
            		 courseEpisodesListLink(courseId));
         } else {
             return EntityModel.of(episode, selfLink(courseId,episode));
         }
     }

}
