package org.miage.userservice.boundary.courses;

import java.util.List;
import java.util.Optional;

import org.miage.userservice.entity.courses.Course;
import org.miage.userservice.entity.courses.*;
import org.miage.userservice.entity.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(collectionResourceRel="course")

public interface CourseResource extends JpaRepository<Course, String>{
	//PUT, GET, POST, DELETE, PATCH sont gérées automatiquement
	
	Iterable<Course> findByNom(@Param("nom") String  nom);
	
	Iterable<Course> findByTheme(@Param("theme") String  theme);

	Iterable<Course> findByNomAndTheme(@Param("nom")String nom, @Param("theme")String theme);

	Iterable<Course> findAllByUtilisateurs_Id(@Param("userId") String userId);

	Iterable<Course> findAllByNomAndThemeAndPrixAndStatutIn(@Param("nom") String nom,@Param("theme") String theme,@Param("prix") Double prix,@Param("statut") List<String> statut);

	Iterable<Course> findAllByNomAndThemeAndStatutIn(@Param("nom") String nom,@Param("theme") String theme,@Param("statut") List<String> statut);

	Iterable<Course> findAllByNomAndStatutIn(String nom, List<String> statut);

	Iterable<Course> findByNomAndThemeAndPrix(String nom, String theme, Double prix);

	Iterable<Course> findAllByThemeAndStatutIn(String theme, List<String> statut);

	Iterable<Course> findAllByStatutIn(List<String> statut);

	Iterable<Course> findByNomOrTheme(String nom, String theme);

}

