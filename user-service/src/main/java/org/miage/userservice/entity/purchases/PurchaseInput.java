package org.miage.userservice.entity.purchases;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseInput {
	
    @Size(max=16)
    @Pattern(regexp = "[0-9]+")
    private String numeroCarte;
    @Size(max=4)
    @Pattern(regexp = "[0-9]+")
    private String code;
    @Size(max=8)
    @Pattern(regexp = "[0-9]+")
    private String codeValidation;
	
}
