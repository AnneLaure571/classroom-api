package org.miage.userservice.entity.courses;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseInput {
	
    @NotNull
    private String nom;
    @Size(min=2, max=100)
    private String theme;
    @NotNull
    @Size(min=2, max=350)
    private String description;
    @NotNull
    @NotBlank
    @Pattern(regexp = "Disponible|Supprimé", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String statut;
    @DecimalMin(value = "0.0", message = "Le montant ne peut pas être inférieur à 0")
	private Double prix;
	@Range(min = 0, max = 10, message = "Note comprise entre 0 et 10") 
	private Double note;
	
}
