package org.miage.userservice.entity.users;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.miage.userservice.core.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInput {
	
    @NotNull
    @NotBlank
    private String nom;
    @Size(min=2)
    private String prenom;
    @NotNull
    @NotBlank
    private String email;
    @Size(min=3)
    private String pays;
	
}
