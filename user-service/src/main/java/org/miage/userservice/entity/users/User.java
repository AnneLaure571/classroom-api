package org.miage.userservice.entity.users;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.miage.userservice.core.UserStatusEnum;
import org.miage.userservice.entity.courses.Course;
import org.miage.userservice.entity.episodes.Episode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
/* permet de générer automatiquement les getters et setters*/
@NoArgsConstructor //Attention, INDISPENSABLE pour JPA !
@AllArgsConstructor
public class User implements Serializable{
	
	private static final long serialVersionUID = 1234567854567L;
	
	@Id
	//TODO remove this for user UID
	//@GeneratedValue(strategy= GenerationType.IDENTITY)
	private String id;
	private String nom;
	private String prenom;
	private String email;
	private String role;
	private UserStatusEnum statut;
	private String pays;
	
	@JsonIgnore
	@ManyToMany
	private List<Course> cours = new ArrayList<>();
	
	@JsonIgnore
	@ManyToMany
	private List<Episode> episodesVus = new ArrayList<>();
	
	public void addEpisode(Episode episode) {
		this.episodesVus.add(episode);
	}
	
	public boolean containsCourse(String id){
        return this.cours.stream().filter(c -> c.getId().equals(id)).findFirst().isPresent();
    }

	public User(String id, String nom, String prenom, String email, String role, UserStatusEnum status, String pays) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.role = role;
		this.statut = status;
		this.pays = pays;
	}

}
