package org.miage.userservice.entity.purchases;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.stereotype.Service;

@Service
public class PurchaseValidator {
	
	private Validator validator;

	PurchaseValidator(Validator validator) {
		this.validator = validator;
	}
	
	public void validate(PurchaseInput course) {
		Set<ConstraintViolation<PurchaseInput>> violations = validator.validate(course);
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
	}

}