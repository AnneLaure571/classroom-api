package org.miage.userservice.entity.courses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.miage.userservice.core.StatusPaymentEnum;
import org.miage.userservice.entity.episodes.Episode;
import org.miage.userservice.entity.users.User;

import com.fasterxml.jackson.annotation.JsonIgnore;

//import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Course implements Serializable{
	
	private static final long serialVersionUID = 1234567854567L;
	
	@Id
	private String id;
	private String nom;
	private String theme;
	private String description;
	private String statut;
	private Double prix;
	private Double note;
	@JsonIgnore
	@OneToMany(mappedBy = "course", fetch = FetchType.LAZY) /*(cascade = CascadeType.ALL, fetch = FetchType.EAGER) /*(fetch=FetchType.LAZY)*/
	//@JsonBackReference
    private List<Episode> episodes = new ArrayList<Episode>();
	
	@JsonIgnore
	@ManyToMany(mappedBy = "cours")
	/*@JoinTable(
		  name = "course_user", 
		  joinColumns = @JoinColumn(name = "userId"), 
		  inverseJoinColumns = @JoinColumn(name = "courseId"))*/
	private List<User> utilisateurs = new ArrayList<User>();
	
	public Course(String id, String nom, String theme, String description, String statut,
			Double prix, Double note) {
		this.id = id;
		this.nom = nom;
		this.theme = theme;
		this.description = description;
		this.statut = statut;
		this.prix = prix;
		this.note = note;
	}
	
	public void addEpisodes(Episode episode) {
		this.episodes.add(episode);
	}
	
	public boolean feeBased(){
        return this.prix > 0;
    }


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}
	
	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public Double getNote() {
		return note;
	}

	public void setNote(Double note) {
		this.note = note;
	}

	public List<Episode> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<Episode> episodes) {
		this.episodes = episodes;
	}

}
