package org.miage.userservice.entity.users;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.springframework.stereotype.Service;

@Service
public class UserValidator {

  private Validator validator;

  UserValidator(Validator validator) {
    this.validator = validator;
  }

  public void validate(UserInput user) {
    Set<ConstraintViolation<UserInput>> violations = validator.validate(user);
    if (!violations.isEmpty()) {
      throw new ConstraintViolationException(violations);
    }
  }
}


