package org.miage.userservice.entity.purchases;

import lombok.Data;

@Data
public class Purchase {
    private String numeroCarte;
    private String code;
    private String codeValidation;
}

