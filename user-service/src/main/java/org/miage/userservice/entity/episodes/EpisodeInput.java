package org.miage.userservice.entity.episodes;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.miage.userservice.entity.courses.Course;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpisodeInput {
	
	@NotNull
	private String titre;
	@NotNull
	@NotBlank
	private String url;
	@Pattern(regexp = "Disponible|Supprimé")
    private String statut;
	@NotNull
    private Course course;

}
