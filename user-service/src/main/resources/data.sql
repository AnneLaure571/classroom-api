---- Users ----
-- INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('c0b7a868-5372-11eb-ae93-0242ac130002', 'Ingals', 'Albert', 'albert-ingals@gmail.com', 'ADMIN', 'mauvais payeur', 'France');
-- INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('cf3b22b6-5372-11eb-ae93-0242ac130002', 'Ingals', 'Laura', 'laura-ingals@gmail.com', 'USER', 'bon(ne) élève', 'France');
-- INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('d364e3ae-5372-11eb-ae93-0242ac130002', 'Ingals', 'Alicia', 'alicia-ingals@gmail.com', 'USER', 'validé', 'France');
-- INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('d7e4aafe-5372-11eb-ae93-0242ac130002', 'Olson', 'Nelly', 'nelly-olson@gmail.com', 'USER', 'validé', 'France');

INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('c0b7a868-5372-11eb-ae93-0242ac130002', 'Ingals', 'Albert', 'albert-ingals@gmail.com', 'ADMIN', 2, 'France');
INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('cf3b22b6-5372-11eb-ae93-0242ac130002', 'Ingals', 'Laura', 'laura-ingals@gmail.com', 'USER', 1, 'France');
INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('d364e3ae-5372-11eb-ae93-0242ac130002', 'Ingals', 'Alicia', 'alicia-ingals@gmail.com', 'USER', 0, 'France');
INSERT INTO user (id, nom, prenom, email, role, statut, pays) VALUES ('d7e4aafe-5372-11eb-ae93-0242ac130002', 'Olson', 'Nelly', 'nelly-olson@gmail.com', 'USER', 0, 'France');

---- COURSES ----
INSERT INTO course (id, nom, description, theme, statut, prix, note) 
VALUES ('de5efccc-5372-11eb-ae93-0242ac130002', 'Kubernetes', 'Cours pour les débutants', 'DevOps', 'Disponible', 0.0, 8.0);
INSERT INTO course (id, nom, description, theme, statut, prix, note) 
VALUES ('de6efccc-5372-11eb-ae93-0241ac130002', 'Docker', 'Cours sur Docker pour amateurs', 'DevOps', 'Disponible', 10.0, 7.5);

---- EPISODES ----
INSERT INTO episode(id, titre, url, statut, course_id) 
VALUES ('de5efccc-5245-11eb-ae93-0241ac130002', 'Introduction', 'https://www.youtube.com/watch?v=MAlSjtxy5ak', 'Disponible', 'de5efccc-5372-11eb-ae93-0242ac130002');
INSERT INTO episode(id, titre, url, statut, course_id) 
VALUES ('a0959cf9-b5d3-4928-ae20-cde0ef30109b', 'Concepts de base', 'https://www.youtube.com/watch?v=HluANRwPyNo', 'Disponible', 'de5efccc-5372-11eb-ae93-0242ac130002');
INSERT INTO episode(id, titre, url, statut, course_id) 
VALUES ('4d176af1-e6b8-4838-b95e-6ec710eaf532', 'Concepts avancés', 'https://www.youtube.com/watch?v=pKO9UjSeLew', 'Disponible', 'de5efccc-5372-11eb-ae93-0242ac130002');
INSERT INTO episode(id, titre, url, statut, course_id) 
VALUES ('a5e91381-0121-46d6-850b-7befa297a144', 'Création d''un cluser', 'https://www.youtube.com/watch?v=rR4n-0KYeKQ', 'Disponible', 'de5efccc-5372-11eb-ae93-0242ac130002');
INSERT INTO episode(id, titre, url, statut, course_id) 
VALUES ('43179c15-6ef7-4fbd-a9c9-ccc36bcafcc2', 'Bonnes pratiques', 'https://www.youtube.com/watch?v=vT3GUKuAzIs', 'Disponible', 'de5efccc-5372-11eb-ae93-0242ac130002');

INSERT INTO episode(id, titre, url, statut, course_id) 
VALUES ('32179c15-6ef7-4fbd-a9c9-ccc36bcafcc2', 'Introduction à Docker', 'https://www.youtube.com/watch?v=vT3GUKuAzIs', 'Disponible', 'de6efccc-5372-11eb-ae93-0241ac130002');