# User-service

## Architecture

Le service est divisé en deux parties

* la partie gérant les utilisateurs :
    * UserRepresentation : controller de l'api avec les endpoints, les méthodes;
    * UserResource : repository (ou interface) jpa permettant l'utilisation des méthodes.
* la partie gérant les cours :
    * CourseRepresentation : controller de l'api avec les endpoints, les méthodes;
    * CourseResource : repository (ou interface) jpa permettant l'utilisation des méthodes;
    * EpisodeResource : repository (ou interface) jpa permettant l'utilisation des méthodes.

## Démarrer le service

### Générer le .jar

Pour passer les tests unitaires :

```
mvn -DskipTests clean install
```

sinon 

```
mvn clean install
```

### Tester

`port : 8085`

```
java -jar user-service-v1.jar
```

## Accéder au service

* Pour importer les routes du service administration sur Postman en local, aller sur l'URL [Open API Admin via Postman](http://localhost:8085/v3/api-docs/)

`OU` 

Y accéder via la collection en ligne :

[![cliquez ici](https://run.pstmn.io/button.svg)](https://www.getpostman.com/collections/ce039eb8dc34c121dc75) 

`OU`

* [Open API Admin via Swagger](http://localhost:8085/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#)

Ce service comporte toutes les fonctionnalités auxquelles un utilisateur a accès sur l'API de cours en ligne. Toutes informations concernant la conception (entity, boundary etc...) sont détaillées dans le rapport au format `.docx` ou `.pdf`.

## DockerFile

Il est possible de lancer à l'aide de `Docker`, le service administration via le Dockerfile.


## Liste des fonctionnalités des utilisateurs

### Se connecter

`Méthode : login, Type : GET`

Cette méthode sert de "pseudo" connexion, en utilisant l'email de l'utilisateur qui est son identifiant pour accéder à la plateforme.

* URL : `https://localhost:8085/utilisateurs/login?email=` ou `{{baseUrl}}/utilisateurs/login?email=`

Exemple :

```

USER :
{{baseUrl}}/utilisateurs/login?email=alicia-ingals@gmail.com

```

Avec HATOES, il y a une méthode spécifique pour la méthode `login` car, par rapport aux autres méthodes `userToResource`, celle-ci est juste utilisé pour afficher les liens qui permettent la gestion de l'API (étant donné que ce service est réservé aux administrateurs). On vérifie que l'utilisateur qui tente de s'identifier possède bien les droits utilisateurs, auquel cas, on retourne une badRequest avec pour message, que l'utilisateur ne dispose pas des accès suffisants.

Avec HATOES, il y a cinq liens qui sont affichés :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour modifier son profil,
* un pour accéder à la collection des cours suivis,
* un pour accéder à la collection des épisodes visionnés,
* un pour accéder au catalogie des cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Modifier son profil": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Cours-suivis": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/cours"
    },
    "Épisodes vus": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/episodes-vus"
    },
    "Catalogue cours": {
        "href": "http://localhost:8085/cours"
    }
}
```

Le lien renvoie vers le profil de l'utilisateur, ce qui permet d'accéder directement au profil de l'utilisateur selectionné (`status OK`). Si l'utilisateur ne possède pas les droits nécessaires, il y aura une réponse `status BAD_REQUEST` si l'email ne retourne aucun utilisateur.

## Accéder à son profil utilisateur

`Méthode : getProfile, Type : GET`

* URL : `https://localhost:8085/utilisateurs/:userId` ou `{{baseUrl}}/utilisateurs/:userId`

Cette méthode permet de visionner son profil pour un utilisateur (voir son compte).

Exemple avec un utilisateur du script :

```

http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002

```

On a repris les mêmes les liens définis avec HATOES pour la méthode `login`.

Avec HATOES, il y a cinq liens qui sont affichés :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour modifier son profil,
* un pour accéder à la collection des cours suivis,
* un pour accéder à la collection des épisodes visionnés,
* un pour accéder au catalogie des cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Modifier son profil": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Cours-suivis": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/cours"
    },
    "Épisodes vus": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/episodes-vus"
    },
    "Catalogue cours": {
        "href": "http://localhost:8085/cours"
    }
}
```

Le lien renvoie vers le profil de l'utilisateur, ce qui permet d'accéder directement au profil de l'utilisateur selectionné (`status OK`). Si l'utilisateur ne possède pas les droits nécessaires, il y aura une réponse `status 400 NOT_FOUND` si l'email ne retourne aucun utilisateur.


### S'inscrire sur la plateforme

`Méthode : subscribeUser, Type : POST`

Cette méthode permet à un nouvel utilisateur de s'inscire sur le site de cours.

* URL : `https://localhost:8085/utilisateurs` ou `{{baseUrl}}/utilisateurs`

Par défaut, le rôle de l'utilisateur est de type `USER` et le statut est `NOUVEAU`. Contrairement au service `administration`, l'administrateur peut configurer le rôle.

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "email": "willy-olson@gmail.com",
    "nom": "Olson",
    "prenom": "Willy",
    "pays": "France"
}
```

L'affichage des liens HATOES est possible car on retourne dans la méthode subscribeUser, une réponse HTTP `201 created` avec l'affichage du body + les liens HATOES.

```
ResponseEntity.status(201).header("location", location.toString()).body(userToResource(saved));
```

Avec HATOES, il y a cinq liens qui sont affichés :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour modifier son profil,
* un pour accéder à la collection des cours suivis,
* un pour accéder à la collection des épisodes visionnés,
* un pour accéder au catalogie des cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Modifier son profil": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Cours-suivis": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/cours"
    },
    "Épisodes vus": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/episodes-vus"
    },
    "Catalogue cours": {
        "href": "http://localhost:8085/cours"
    }
}
```

Si l'email existe déjà, on informera en retournant une erreur de conflit (`409 conflict`) avec pour message dans le body, "Un utilisateur existe déjà avec cet email !", sinon si les champs sont vides, un exception.

### Modifier son profil

`Méthode : updateProfile, Type : PATCH`

* URL : `https://localhost:8085/utilisateurs/:userId` ou `{{baseUrl}}/utilisateurs/:userId`

Vous pouvez tester avec l'exemple ci-dessous :

```
{
    "pays": "Amérique"
}
```

avec l'id d'utilisateur suivant : d364e3ae-5372-11eb-ae93-0242ac130002.

On récupère le contenu du body (d'où l'utilisation de la Map), car il y peut y avoir un ou plusieurs champs.
L'affichage est possible car on retourne dans la méthode updateUserPartiel, une réponse HTTP `200 OK` avec l'affichage du body + les liens HATOES, l'utilisateur a bien été modifié.

```
ResponseEntity.status(201).header("location", location.toString()).body(userToResource(saved));
```

Avec HATOES, il y a cinq liens qui sont affichés :

* un pour accéder au profil d'un utilisateur en particulier,
* un pour modifier son profil,
* un pour accéder à la collection des cours suivis,
* un pour accéder à la collection des épisodes visionnés,
* un pour accéder au catalogie des cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Modifier son profil": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f"
    },
    "Cours-suivis": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/cours"
    },
    "Épisodes vus": {
        "href": "http://localhost:8085/utilisateurs/2153c98a-054b-474f-a88c-27296ece2e4f/episodes-vus"
    },
    "Catalogue cours": {
        "href": "http://localhost:8085/cours"
    }
}
```

Si le body est vide ou que l'utilisateur n'exsite pas ou qu'il a été supprimé, il y aura une réponse badRequest  `404 BAD_REQUEST` (body invalide) ou `400 NOT_FOUND` (pas d'utilisateur correspondant).

### Consulter sa liste des cours

`Méthode : getAllCoursesSubUser, Type : GET`

* URL : `https://localhost:8085/utilisateurs/:userId` ou `{{baseUrl}}/utilisateurs/:userId`

Cette méthode permet de consulter la liste des cours achetés.

Exemple avec un utilisateur du script :

```
http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002

```

Avec HATOES, il y a trois liens qui sont affichés :

* un pour accéder au cours,
* un pour modifier son profil,
* un pour accéder à la collection des cours suivis.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    }
}
....
"_links": {
    "Cours-suivis": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours"
    },
    "Utilisateur": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002"
    }
}
```

On a réponse avec un statut `200 OK`.

## Consulter un cours de la liste 

`Méthode : getCourseSubscribed, Type : GET`

* URL : `https://localhost:8085/utilisateurs/:userId/cours/:courseId` ou `{{baseUrl}}/utilisateurs/:userId/cours/:courseId`

Exemple avec un utilisateur et un cours du script :

```
http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002
{{baseUrl}}/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002

```

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à un cours en particulier,
* un pour accéder à la liste des épisodes du cours,
* un pour se désinscrire du cours,
* un pour accéder à la collection des cours.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Episodes": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Se désinscrire du cours": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/unsubscribe?courseId=de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Cours-suivis": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours"
    }
}
```

S'il y a une correspondance avec les ids, on retourne les informations du cours (`status OK`) sinon on retourne une réponse `404 NOT_FOUND` si rien n'est trouvé (l'utilisateur a été supprimé ou n'existe pas). S'il y a une erreur, on retourne une badRequest `400 BAD_REQUEST` avec un message d'erreur (il n'y a pas de correspondance).

### S'inscrire à un cours

`Méthode : subscribeCourse, Type : GET`

* URL : `https://localhost:8085/cours/:courseID/subscribe?userId=` ou `{{baseUrl}}/utilisateurs/cours/:courseID/subscribe?userId=`

Exemple avec un utilisateur et un cours du script :

```
http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0242ac130002/subscribe?userId=d364e3ae-5372-11eb-ae93-0242ac130002
{{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/subscribe?userId=d364e3ae-5372-11eb-ae93-0242ac130002

```

Carte bancaire :
```
{
    "numeroCarte": "497010123456",
    "code": "253",
    "codeValidation": "2536"
}

```

Dans le header, on retourne l'URL qui permet à l'utilisateur accéder à sa liste de cours suivis, on lui indique également par message dans le body, que is le paiement se passe bien, le cours a été ajouté à sa liste.

```
http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours
```

S'il y a une correspondance avec les ids, on retourne les informations du cours (`status OK`) sinon si l'utilisateur est déjà inscrit, on retourne une réponse `409 Conflict`. S'il y a une erreur avec les données saisies, on retourne une badRequest `400 BAD_REQUEST` avec un message d'erreur, il y a un par erreur, ce qui fait qu'il y a une réponse BadRequest si la carte n'est pas valide et si les informations ne respectent pas le bon format JSON. 

### Se désinscrire d'un cours 

`Méthode : unsubscribeCourse, Type : DELETE`

* URL : `https://localhost:8085/utilisateurs/:userId/cours/:courseId` ou `{{baseUrl}}/utilisateurs/:userId/cours/:courseId`

Exemple avec un utilisateur et un cours du script :

```
http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/unsubscribe?courseId=de5efccc-5372-11eb-ae93-0242ac130002
{{baseUrl}}/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/unsubscribe?courseId=de5efccc-5372-11eb-ae93-0242ac130002

```

Avec HATOES, il y a un lien qui est généré :

* un pour accéder à la collection des cours suivis.

```
{
    "rel": "Cours-suivis",
    "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours"
}
```

S'il y a une correspondance avec les ids, on retourne les informations du cours (`status OK`) sinon on retourne une réponse `404 NOT_FOUND` si rien n'est trouvé (l'utilisateur a été supprimé ou n'existe pas). S'il y a une erreur, on retourne une badRequest `400 BAD_REQUEST` avec un message d'erreur (il n'y a pas de correspondance).

## Accéder à la liste des épisodes du cours acheté 

`Méthode : getCourseEpisodesSubscribed, Type : GET`

* URL : `https://localhost:8085/utilisateurs/:userId/cours/:courseId/episodes` ou `{{baseUrl}}/utilisateurs/:userId/cours/:courseId/episodes`

Exemple avec un utilisateur et un cours du script :

```
http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes
{{baseUrl}}/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes

```

Pour tester, il faut penser à ajouter le cours et ensuite, il est possible de visionner l'épisode.

Avec HATOES, il y a trois liens :

* un pour visionner à l'épisode visionné,
* un pour accéder à la collection des épisodes visionnées,
* un pour accéder au cours.

```
"_links": {
    "Visionner l'épisode": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/43179c15-6ef7-4fbd-a9c9-ccc36bcafcc2/view"
    }
}
....
"_links": {
    "Liste des épisodes": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    },
    "Cours": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours"
    }
}
```

S'il y a une correspondance avec les ids, on retourne la liste des épisodes du cours (`status OK`) sinon on retourne une réponse `404 NOT_FOUND` si rien n'est trouvé (l'utilisateur a été supprimé ou n'existe pas). S'il y a une erreur, on retourne une badRequest `400 BAD_REQUEST` avec un message d'erreur (il n'y a pas de correspondance car les informations ne sont pas correctes ou qu'il n'y a pas de correspondance entre l'utilisateur et l'episode).

## Accéder à sa liste d'épisodes visionnées

`Méthode : getAllViewedEpisodes, Type : GET`

* URL : `https://localhost:8085/utilisateurs/:userId/episodes-vus` ou `{{baseUrl}}/utilisateurs/:userId/episodes-vus`

Cette méthode permet à un utilisateur de voir les épisodes qu'il a visionné (tout cours confondu).

Exemple avec un utilisateur du script :

```

http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus
{{baseUrl}}/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus

```

Avec HATOES, il y a trois liens qui sont affichés :

* un pour accéder à l'épisode,
* un pour accéder au profil de l'utilisateur,
* un pour accéder à la collection des cours suivis.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    }
}
....
"_links": {
    "Cours-suivis": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours"
    },
    "Utilisateur": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002"
    }
}
```

On a une réponse `OK 200` qui renvoie la liste des épisodes visionnés.


## Accéder à un épisode visionné

`Méthode : getEpisodeViewed, Type : GET`

* URL : `https://localhost:8085/utilisateurs/:userId/episodes-vus/:episodeId` ou `{{baseUrl}}/utilisateurs/:userId/episodes-vus/:episodeId`

Exemple avec un utilisateur et un cours du script :

```
http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus/de5efccc-5245-11eb-ae93-0241ac130002
{{baseUrl}}/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus/de5efccc-5245-11eb-ae93-0241ac130002

```

Pour tester, il faut penser à ajouter le cours et ensuite, il est possible de visionner l'épisode.

Avec HATOES, il y a deux liens :

* un pour accéder à l'épisode visionné (selflink),
* un pour accéder à la collection des épisodes visionnées.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Épisodes vus": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus"
    }
}
```

S'il y a une correspondance avec les ids, on retourne les informations de l'épisode (`status OK`) sinon on retourne une réponse `404 NOT_FOUND` si rien n'est trouvé (l'utilisateur a été supprimé ou n'existe pas). S'il y a une erreur, on retourne une badRequest `400 BAD_REQUEST` avec un message d'erreur (il n'y a pas de correspondance car les informations ne sont pas correctes ou qu'il n'y a pas de correspondance entre l'utilisateur et l'episode).

## Regarder un épisode

`Méthode : addViewedEpisode, Type : PUT`

* URL : `https://localhost:8085/utilisateurs/:userId/episodes-vus/:episodeId` ou `{{baseUrl}}/utilisateurs/:userId/episodes-vus/:episodeId`

Exemple avec un utilisateur et un cours du script :

```
http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002/view
{{baseUrl}}/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002/view
```

Pour tester, il faut penser à ajouter le cours et ensuite, il est possible de visionner l'épisode.

Avec HATOES, il y a deux liens :

* un pour accéder à l'épisode visionné (selflink),
* un pour accéder à la collection des épisodes visionnées.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Épisodes vus": {
        "href": "http://localhost:8085/utilisateurs/d364e3ae-5372-11eb-ae93-0242ac130002/episodes-vus"
    }
}
```

S'il y a une correspondance avec les ids, on retourne les informations de l'épisode (`status OK`) sinon on retourne une réponse `404 NOT_FOUND` si rien n'est trouvé (l'utilisateur a été supprimé ou n'existe pas). S'il y a une erreur, on retourne une badRequest `400 BAD_REQUEST` avec un message d'erreur (il n'y a pas de correspondance car les informations ne sont pas correctes ou qu'il n'y a pas de correspondance entre l'utilisateur, le cours et l'épisode).


## Liste des fonctionnalités des cours & épisodes

### Lister tous les cours

`Méthode : getAllCourses, Type : GET`

Cette méthode permet de lister l'ensemble des cours du site.

* URL : `https://localhost:8085/cours` ou `{{baseUrl}}/cours`

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour réaliser une recherche avec les paramètres.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002/episodes"
    }
}
...
"_links": {
    "Liste des cours": {
        "href": "http://localhost:8085/cours"
    },
    "Rechercher un cours": {
        "href": "http://localhost:8085/cours/search{?statut,nom,theme,prix}",
        "templated": true
    }
}
```

On retourne la liste des cours, ce qui correspond à une réponse HTTP `200 OK`.

### Accéder aux ressources d'un cours en particulier

`Méthode : getCourse, Type : GET`

* URL : `https://localhost:8085/cours/:courseId` ou `{{baseUrl}}/cours/:courseId`

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour s'inscrire au cours.


Exemple, vous pouvez essayer avec un ID fixe d'un cours (le cours est dans le script de peuplement de la base) -> `de5efccc-5372-11eb-ae93-0242ac130002`.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002/episodes"
    },
    "S'inscrire au cours": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002/subscribe?userId={userId}",
        "templated": true
    },
    "Liste des cours": {
        "href": "http://localhost:8085/cours"
    }
}
```

S'il y a une correspondance avec l'id, on retourne les informations du cours (`status OK`) sinon on retourne une réponse `404 NOT_FOUND`.

### Rechercher des cours

`Méthode : searchCourse, Type : GET`

* URL : `https://localhost:8085/cours/?statut=Supprimé&statut=Disponible&nom=Docker&theme=DevOps&prix=10.0` ou `{{baseUrl}}/cours/?statut=Supprimé&statut=Disponible&nom=Docker&theme=DevOps&prix=10.0`

Cette méthode permet de rechercher un ou plusieurs utilisateurs à partir des champs suivants :

* statut : nécessite un paramètre de type `String`, ex : Disponible,
* nom : nécessite un paramètre de type `String`, ex : Kubernetes,
* theme : nécessite un paramètre de type `String`, ex : DevOps,
* prix : nécessite un paramètre de type `Double`, ex : 0.0.

Si vous souhaitez tester avec plusieurs statuts, il faut le faire de la manière suivante :

* {{baseUrl}}/cours/?statut=Disponible&statut=Supprimé

* URL pour tester : `https://localhost:8085/cours/?statut=Disponible&nom=Docker` ou `{{baseUrl}}/cours/?statut=Disponible&nom=Docker`

la liste des différentes recherches possibles :

* findAllByNomAndThemeAndPrixAndStatutIn(nom, theme, prix, statut);
* findAllByNomAndThemeAndStatutIn(nom, theme, statut);
* findByNomAndThemeAndPrix(nom, theme, prix);
* findByNomAndTheme(nom, theme);
* findAllByNomAndStatutIn(nom, statut);
* findAllByThemeAndStatutIn(theme, statut);
* findAllByThemeAndStatutIn(theme, statut);
* findAllByStatutIn(statut);
* findByNomOrTheme(nom,theme);

Avec un ou plusieurs statut, un nom, un thème et un prix.

Avec HATOES, il y a quatre liens qui sont générés :

* un pour accéder à la collection des cours,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder à un cours en particulier,
* un pour s'inscrire au cours.


Exemple, vous pouvez essayer avec un ID fixe d'un cours (le cours est dans le script de peuplement de la base) -> `de5efccc-5372-11eb-ae93-0242ac130002`.

```
"_links": {
    "self": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002/episodes"
    },
    "S'inscrire au cours": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0241ac130002/subscribe?userId={userId}",
        "templated": true
    },
    "Liste des cours": {
        "href": "http://localhost:8085/cours"
    }
}
```

On retourne une liste de cours correspondants aux filtres (`status OK`).
Si aucun cours n'est trouvé, il retourne un URI avec la collection des cours (/cours) qui permet d'accéder à la ressource qui liste tous les cours (`status NOT_FOUND`).

### Lister tous les épisodes d'un cours

`Méthode : getAllEpisodesCourse, Type : GET`

Cette méthode permet de lister l'ensemble des épisodes d'un cours (`status 200 OK`).

* URL : `https://localhost:8085/cours/:courseId/episodes` ou `{{baseUrl}}/cours/:courseId/episodes`

* `URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes) 

Avec HATOES, il y a trois liens qui sont générés :

* un pour accéder à un épisode en particulier,
* un pour accéder à la liste des épisodes du cours,
* un pour accéder au cours.


```
"_links": {
    "self": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/43179c15-6ef7-4fbd-a9c9-ccc36bcafcc2"
    }
}
...
"_links": {
    "Liste des épisodes": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    },
    "Cours": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0242ac130002"
    }
}
```

### Accéder aux ressources d'un épisode en particulier

`Méthode : getEpisode, Type : GET`

Cette méthode permet de lister l'ensemble des épisodes d'un cours.

* URL : `https://localhost:8085/cours/:courseId/episodes` ou `{{baseUrl}}/cours/:courseId/episodes`

* `URL de test` : [test]({{baseUrl}}/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002) 

Avec HATOES, il y a deux liens qui sont générés :

* un pour accéder à un cours en particulier,
* un pour accéder à la liste des épisodes du cours.

Exemple, vous pouvez essayer avec un ID fixe d'un cours (le cours est dans le script de peuplement de la base) -> `de5efccc-5372-11eb-ae93-0242ac130002` et un pour l'épisode -> `de5efccc-5245-11eb-ae93-0241ac130002`

```
"_links": {
    "self": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes/de5efccc-5245-11eb-ae93-0241ac130002"
    },
    "Liste des épisodes": {
        "href": "http://localhost:8085/cours/de5efccc-5372-11eb-ae93-0242ac130002/episodes"
    }
}
```

Si le cours est retrouvé, on affiche son contenu (`status OK`) sinon on retourne une réponse indiquant que la ressource n'existe pas (`status NOT_FOUND`).

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
